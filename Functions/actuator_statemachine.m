function [ControlWord, DriveShouldBeEnabled, LatchJointError_Rising] = ...
    actuator_statemachine(EnableRequest, DisableRequest, UseActuator, ...
                        JointErrorIn, AutoResetErrorInterval_Off_ms, ...
                        AutoResetErrorInterval_On_ms, Time)
%ACTUATOR_STATEMACHINE Manage the state of an actuator
%   This used to be a Stateflow diagram, but it got turned back into code.

% The different states are:
STATE_OFF = 1;
STATE_READY_TO_SWITCH_ON = 2;
STATE_SWITCHED_ON = 3;
STATE_OPERATION_ENABLED = 4;

% CtrlWord values are:      (See reference manual)
DEC_SHUTDOWN = uint16(6); % Enable voltage + quick stop
DEC_FAULT_RESET = uint16(128); % Fault reset (rising edge)
DEC_SWITCH_ON = uint16(7); % Switch on + enable voltage + quick stop
DEC_OPERATION = uint16(15); % Operation + switch on + enable voltage + quick stop

% Constants:
DURATION_READY = 0.020; % Time in the READY_TO_SWITCH_ON state
DURATION_SWITCHED_ON = 0.150; % Time in the SWITCHED_ON state

%% Persistent variables and initialization
persistent state_; % Current state
persistent state_start_; % Time when this state was entered
persistent latch_error_; % True if error should be latched (done externally)
persistent last_error_reset_; % Time of most recent error reset

if isempty(state_)
    state_ = STATE_OFF; % Initial state
    state_start_ = Time;
    latch_error_ = 0;
    last_error_reset_ = Time;
end

new_state = state_; % Use variable instead of directly writing to `state`
state_time = Time - state_start_;

%% State guards
% We first evaluate necessary state changes such that a new state is
% processed this frame, instead of in the next.

% Turn off drives (from any state)
if state_ ~= STATE_OFF
    % Drive can be disabled from anywhere
    if DisableRequest || ~UseActuator || JointErrorIn
        new_state = STATE_OFF;
        
        latch_error_ = 1; % Start latch
    end
end

switch state_
    
    case STATE_OFF
        if UseActuator && EnableRequest
            new_state = STATE_READY_TO_SWITCH_ON;
        end

    case STATE_READY_TO_SWITCH_ON
    
        if state_time > DURATION_READY
            new_state = STATE_SWITCHED_ON;
        end
        
    case STATE_SWITCHED_ON
        if state_time >= DURATION_SWITCHED_ON
            new_state = STATE_OPERATION_ENABLED;
        end

    case STATE_OPERATION_ENABLED
        if EnableRequest
            % Re-enable the drive
            new_state = STATE_READY_TO_SWITCH_ON;
        end
end

%% State transitions

if new_state ~= state_
    
    state_ = new_state;
    state_start_ = Time;
    state_init = true;
    
    state_time = 0; % Reset time in current state
    
    % Also reset the fault-reset timer to prevent a restet instantly in the
    % new state
    last_error_reset_ = Time;
    
else
    state_init = false;
end


switch state_ 
    
%% State: Off
    
    case STATE_OFF
    
        ControlWord = DEC_SHUTDOWN;

        % Clear latch after at least one frame
        if latch_error_ && state_time > 0.010
            latch_error_ = 0; % Reset latch
        end

        % Try to reset the drive errors at a fixed interval (only when in error)
        if UseActuator && AutoResetErrorInterval_Off_ms > 0 && JointErrorIn
            if Time - last_error_reset_ >= AutoResetErrorInterval_Off_ms / 1000
                ControlWord = ControlWord + DEC_FAULT_RESET; % Will be overwritten next loop
                last_error_reset_ = Time;
            end
        end

%% State: ReadyToSwitchOn

    case STATE_READY_TO_SWITCH_ON
    
        ControlWord = DEC_SHUTDOWN + DEC_FAULT_RESET;
    

%% State: SwitchedOn

    case STATE_SWITCHED_ON
    
        if state_init
            latch_error_ = 1;
        end

        ControlWord = DEC_SWITCH_ON;


%% State: OperationEnabled

    case STATE_OPERATION_ENABLED
    
        if state_init
            latch_error_ = 0;
        end

        ControlWord = DEC_OPERATION;

        % Try to reset the drive errors at a fixed interval
        if AutoResetErrorInterval_On_ms > 0     % Also reset without errors
            if Time - last_error_reset_ >= AutoResetErrorInterval_On_ms / 1000
                ControlWord = ControlWord + DEC_FAULT_RESET; % Will be overwritten next loop
                last_error_reset_ = Time;
            end
        end
    
    otherwise
        error('Undefined state %d!', state_);
end

%% Output

DriveShouldBeEnabled = state_ == STATE_OPERATION_ENABLED;
LatchJointError_Rising = latch_error_;

end
