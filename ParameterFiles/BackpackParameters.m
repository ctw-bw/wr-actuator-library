% BackpackParameters.m
%
% This script contains parameters for all interfacing in the WE2 backpack.
% We have the following sections:
% - Battery voltage monitoring
%   Input: Analog voltage in ADC counts; 
%   Output: Battery voltage in volts (note there are voltage dividing 
%       resistors in the backpack)
% - Checking state of motor power
%   Input: Analog voltage of the solid state relais control signal in ADC
%       counts
%   Output: Boolean. No parameter here (hardcoded in Simulink model)
% - Foot contact
%   Input: Analog voltage of digital signals from the insole interface print.
%       (Hopefully analog in the future)
%   Output: 
%     + FootContact bus, with various booleans:
%       - HeelContact
%       - ToeContact
%       - HeelOrToeContact
%       - HeelAndToeContact
%     + Numeric output for logging (Bit 0: heel contact; bit 1: toe contact)
% - Crutch buttons
%   Input: Analog voltage of the different buttons
%   Output:
%      + Bus with boolean for each button
%      + Single element with one bit for each button for logging
% - Backpack temperature sensor (Type??)
%   Input: Analog voltage 
%   Output: Temperature in degrees C
WE_Backpack_params.MotorBatteryVoltageGain = 10.01; % Multiply read value (in V) by this to get battery voltage
WE_Backpack_params.LogicBatteryVoltageGain =  5.01; % Multiply read value (in V) by this to get battery voltage

% Buttons have debounce time
WE_Backpack_params.HandbrakePushButton.DebounceTime_s = 0.05;
WE_Backpack_params.HandbrakePushButton.ThresholdVoltage_V = 0.5;
WE_Backpack_params.NearButtonDebounce.DebounceTime_s = 0.05;
WE_Backpack_params.NearButtonDebounce.ThresholdVoltage_V = 0.5;
WE_Backpack_params.FarButtonDebounce.DebounceTime_s = 0.05;
WE_Backpack_params.FarButtonDebounce.ThresholdVoltage_V = 0.5;
WE_Backpack_params.ThumbButtonDebounce.DebounceTime_s = 0.05;
WE_Backpack_params.ThumbButtonDebounce.ThresholdVoltage_V = 0.5;
