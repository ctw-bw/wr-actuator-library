function B = struct2bus(s, varargin)
% STRUCT2BUS    Converts the structure s to a Simulink Bus Object

% Obtain the fieldnames of the structure
sfields = fieldnames(s);

% Loop through the structure
for i = 1:length(sfields)
    % Create BusElement for each field
    elems(i) = Simulink.BusElement; %#ok<*AGROW>
    elems(i).Name = sfields{i};
    
    if islogical(s.(sfields{i}))
        data_type = 'boolean'; % "logical" is not recognized by Simulink
    else
        data_type = class(s.(sfields{i}));
    end
    
    elems(i).Dimensions = mysize(s.(sfields{i}));
    elems(i).DataType = data_type;
    elems(i).SampleTime = -1;
    elems(i).Complexity = 'real';
    elems(i).SamplingMode = 'Sample based';
end

% Create main fields of Bus Object and generate Bus Object in the base
% workspace.
BusObject = Simulink.Bus;
BusObject.HeaderFile = '';
BusObject.Description = sprintf('');
BusObject.Elements = elems;
B = BusObject;

function s = mysize(X)
% MYSIZE
if numel(X) == 1
    s = 1;
elseif size(X, 1) == 1
    s = size(X, 2)';
elseif size(X, 2) == 1
    s = size(X, 1);
else
    s = size(X);
end
