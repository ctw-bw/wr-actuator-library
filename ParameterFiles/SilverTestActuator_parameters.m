% SilverTestActuator_Parameters
%
% This script contains parameters for the silver test actuator.
%
% Positive force is if the robot exerts a abduction/flexion/plantarflexion
% force onto the human (and thus the human exerts an 
% adduction/extension/dorsiflexion force to the robot).
% Zero angle is when 'standing straight'.
%

% Remove all variables we're going to create
clear SilverTestActuator InputBus OutputBus sampleTime;

%% Sample Time
sampleTime = 0.001; % [seconds]

%% Left Hip Abduction 
SilverTestActuator.Motor.RatedCurrent_A = 23; % A
SilverTestActuator.Motor.MaxCurrent_A = 40; % A
SilverTestActuator.Motor.MotorConstant_Nm_A = 0.0562; % Nm/A
SilverTestActuator.GearboxRatio = 100;
SilverTestActuator.SpringStiffness_Nm_rad = 1900; % (?) [Nm/rad]

SilverTestActuator.MotorEncoder.Counts_rev = 8192; % Negative -> Negative encoder count is positive angle
SilverTestActuator.MotorEncoder.SpikeFilter.WindowGrowthRate_rad_sample = 0.003;

SilverTestActuator.JointEncoder.Counts_rev = 2^20;
SilverTestActuator.JointEncoder.Offset = 0; % Encoder does not work yet
SilverTestActuator.JointEncoder.ROMIncludesZero = 1; % The range of motion does not contain encoder reading 0
SilverTestActuator.JointEncoder.SpikeFilter.WindowGrowthRate_rad_sample = 0.003;

SilverTestActuator.SpringEncoder.Counts_rev = 2^20;
SilverTestActuator.SpringEncoder.Offset = 0;  % Encoder does not work yet
SilverTestActuator.SpringEncoder.ROMIncludesZero = 1; % The range of motion containts/comes close to encoder reading 0
SilverTestActuator.SpringEncoder.SpikeFilter.WindowGrowthRate_rad_sample = 0.01;

SilverTestActuator.Limits.MotorAngleMin_rad = -2; % Software endstop; actually the motor angle is limited (not the joint angle)
SilverTestActuator.Limits.MotorAngleMax_rad = 2; % Software endstop; actually the motor angle is limited (not the joint angle). Need to calibrate
SilverTestActuator.Limits.MaxVelocity_rad_s = 3;
SilverTestActuator.Limits.MaxAcceleration_rad_s2 = 20;
SilverTestActuator.Limits.MaxJointTorque_Nm = 30; % Should be 70 in the end, but that is unstable for now
SilverTestActuator.PVALimiter.DampingConstant = 200;

SilverTestActuator.Guards.JointAnglePositionGuardMin_rad = SilverTestActuator.Limits.MotorAngleMin_rad - 0.05;
SilverTestActuator.Guards.JointAnglePositionGuardMax_rad = SilverTestActuator.Limits.MotorAngleMax_rad + 0.05;
SilverTestActuator.Guards.TorqueGuard_Nm = 120;

SilverTestActuator.TorqueControllerType = 3; % DOB controller, do not change
SilverTestActuator.DOBController.SEA_inertia = (0.87262e-4 + 0.112e-4) * ...
    SilverTestActuator.GearboxRatio^2 + 5.0e-4;
SilverTestActuator.DOBController.SEA_tau = 0.5;
SilverTestActuator.DOBController.SEA_damper = SilverTestActuator.DOBController.SEA_inertia / SilverTestActuator.DOBController.SEA_tau;
SilverTestActuator.DOBController.System_Bandwidth_Hz = 30; % Hz
SilverTestActuator.DOBController.System_Zeta = 0.7;
SilverTestActuator.DOBController.DOB_FilterBandwidth_Hz = 10; % Hz
SilverTestActuator.DOBController.DOB_Bandwidth_Hz = SilverTestActuator.DOBController.System_Bandwidth_Hz;
SilverTestActuator.DOBController.DOB_Zeta = SilverTestActuator.DOBController.System_Zeta;
SilverTestActuator.DOBController.DOBGain = 0.7;

SilverTestActuator.DOBController.TorqueFFGain = 1;
SilverTestActuator.DOBController.KpGainCorrection = 1;
SilverTestActuator.DOBController.KdGainCorrection = 1;
SilverTestActuator.DOBController.Derivative_FilterBandwidth_Hz = 160;


SilverTestActuator.AdaptiveMaxMotorTorque.MaxMotorTorqueAtZeroVelocity_Nm = abs(SilverTestActuator.Motor.MaxCurrent_A * SilverTestActuator.Motor.MotorConstant_Nm_A * SilverTestActuator.GearboxRatio); % As a separate parameter so that you can reduce it if this amount gives problems
SilverTestActuator.AdaptiveMaxMotorTorque.MaxMotorTorqueAtMaxVelocity_Nm = 0.5*SilverTestActuator.AdaptiveMaxMotorTorque.MaxMotorTorqueAtZeroVelocity_Nm;
SilverTestActuator.AdaptiveMaxMotorTorque.DecayVelocity_normalized = 0.5; % See comment in DynamicMaxMotorTorque block

