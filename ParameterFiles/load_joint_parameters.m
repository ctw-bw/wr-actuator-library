% Joint_Parameters
%
% This script contains parameters for all actuators of the Wearable Exoskeleton.
% This is the basis for any configuration, so for both WE1 and WE2 (and any
% other as well). For backward compatibilty, I'll still provide a
% WE1_parameters.m, which will get its parameters from this script.
%
% Opposed to WE1, now all parameters are stored in one big struct, called
% Joint_params.
%
% The signs of all values are chosen such that all numbers are positive at:
% - hip abduction (spread legs)
% - hip flexion (knee forward)
% - knee flexion (ankle backward)
% - ankle plantarflexion (toe down)
% Positive force is if the robot exerts a abduction/flexion/plantarflexion
% force onto the human (and thus the human exerts an 
% adduction/extension/dorsiflexion force to the robot).
% Zero angle is when 'standing straight'.
% Maximum interaction torques for current actuator design (spring 1534Nm)
% are 120Nm torque, and 130Nm for spring deflection guard. The second error
% should never trigger, but(!) it guarantees that the spring will not be
% damaged.

% Remove all variables we're going to create
clear Joint_params InputBus OutputBus sampleTime;

%% Sample Time
sampleTime = 0.001; % [seconds]

%% General

% We first create a general struct, that we will assign to each joint and
% overwrite values where needed, to keep it DRY.
% Comments about the meaning of fields should be kept here only.
%
% WARNING: Dependent (computed) parameters will also have to overriden when
% their parent parameters were changed!

Joint = struct;

Joint.Motor.RatedCurrent_A = 23; % [A]
Joint.Motor.MaxCurrent_A = 40; % [A]
Joint.Motor.MotorConstant_Nm_A = 0.0562; % [Nm/A]
Joint.GearboxRatio = 100; % Output torque = GearboxRatio * Input torque
Joint.SpringStiffness_Nm_rad = 1534; % [Nm/rad]

% Set to 2 for the v2 actuators, and 3 for v3.
% This will account for how the encoders are placed inside the actuator.
Joint.Version = 2;

% 'Counts per rev' can be negative to make the angle positive in the right
% direction.
% 'ROMIncludesZero' determines whether the encoder 0 will be encountered
% within the range of motion. When set to 1, the encoder output will be
% unwrapped to avoid a discontinuous output.

Joint.MotorEncoder.Counts_rev = 2^16;
Joint.MotorEncoder.SpikeFilter.WindowGrowthRate_rad_sample = 0.003;

Joint.OutputEncoder.Counts_rev = 2^20;
Joint.OutputEncoder.Offset = 0; % [counts]
Joint.OutputEncoder.ROMIncludesZero = 0;
Joint.OutputEncoder.SpikeFilter.WindowGrowthRate_rad_sample = 0.003;

Joint.SpringEncoder.Counts_rev = 2^20;
Joint.SpringEncoder.Offset = 0; % [counts]
Joint.SpringEncoder.ROMIncludesZero = 0;
Joint.SpringEncoder.SpikeFilter.WindowGrowthRate_rad_sample = 0.01;

% Software endstop, only the motor angle is limited (not the joint angle)
Joint.Limits.MotorAngleMin_rad = -1.0;
Joint.Limits.MotorAngleMax_rad = 1.0;
Joint.Limits.MaxVelocity_rad_s = 100;
Joint.Limits.MaxAcceleration_rad_s2 = 20;
Joint.Limits.MaxJointTorque_Nm = 100; 
Joint.PVALimiter.DampingConstant = 200;

Joint.Guards.JointAnglePositionGuardMin_rad = Joint.Limits.MotorAngleMin_rad - 0.05;
Joint.Guards.JointAnglePositionGuardMax_rad = Joint.Limits.MotorAngleMax_rad + 0.05;
Joint.Guards.TorqueGuard_Nm = 120;
Joint.Guards.SpringDeflectionGuard_rad = (Joint.Guards.TorqueGuard_Nm + 10) / Joint.SpringStiffness_Nm_rad;

Joint.TorqueControllerType = 3; % DOB controller (do not change)
Joint.DOBController.SEA_inertia = (0.87262e-4 + 0.112e-4) * Joint.GearboxRatio^2 + 5.0e-4;
Joint.DOBController.SEA_tau = 0.5;
Joint.DOBController.SEA_damper = Joint.DOBController.SEA_inertia / Joint.DOBController.SEA_tau;
Joint.DOBController.System_Bandwidth_Hz = 30; % Hz
Joint.DOBController.System_Zeta = 0.9;
Joint.DOBController.DOB_FilterBandwidth_Hz = 10; % Hz
Joint.DOBController.DOB_Bandwidth_Hz = Joint.DOBController.System_Bandwidth_Hz;
Joint.DOBController.DOB_Zeta = Joint.DOBController.System_Zeta;
Joint.DOBController.DOBGain = 0.61;

Joint.DOBController.TorqueFFGain = 1;
Joint.DOBController.KpGainCorrection = 1;
Joint.DOBController.KdGainCorrection = 1;
Joint.DOBController.Derivative_FilterBandwidth_Hz = 160;

Joint.AdaptiveMaxMotorTorque.MaxMotorTorqueAtZeroVelocity_Nm = abs(Joint.Motor.MaxCurrent_A * Joint.Motor.MotorConstant_Nm_A * Joint.GearboxRatio);
% As a separate parameter so that you can reduce it if this amount gives problems
Joint.AdaptiveMaxMotorTorque.MaxMotorTorqueAtMaxVelocity_Nm = 0.5 * Joint.AdaptiveMaxMotorTorque.MaxMotorTorqueAtZeroVelocity_Nm;
Joint.AdaptiveMaxMotorTorque.DecayVelocity_normalized = 0.5; % See comment in DynamicMaxMotorTorque block

%% Left Hip Abduction 

LHA = Joint;

LHA.OutputEncoder.Offset = 434950;

LHA.SpringEncoder.Offset = 89333;
LHA.SpringEncoder.ROMIncludesZero = 1; % The range of motion comes close to encoder reading 0

% Mechanical endstops at -0.3684 / 0.3659 rad
LHA.Limits.MotorAngleMin_rad = -0.25;
LHA.Limits.MotorAngleMax_rad = 0.25;

LHA.Guards.JointAnglePositionGuardMin_rad = LHA.Limits.MotorAngleMin_rad - 0.05;
LHA.Guards.JointAnglePositionGuardMax_rad = LHA.Limits.MotorAngleMax_rad + 0.05;

%% Left Hip Flexion

LHF = Joint;

LHF.OutputEncoder.Offset = 169500;
LHF.OutputEncoder.ROMIncludesZero = 1;

LHF.SpringEncoder.Offset = 876700;

% Mechanical endstops at -0.3765 / 1.8865 rad
LHF.Limits.MotorAngleMin_rad = -0.20;
LHF.Limits.MotorAngleMax_rad = 2.4;

LHF.Guards.JointAnglePositionGuardMin_rad = LHF.Limits.MotorAngleMin_rad - 0.05;
LHF.Guards.JointAnglePositionGuardMax_rad = LHF.Limits.MotorAngleMax_rad + 0.05;

%% Left Knee

LK = Joint;

LK.Motor.MotorConstant_Nm_A = -Joint.Motor.MotorConstant_Nm_A;
% Motor seems inverted, set constant to negative to match with convention

LK.GearboxRatio = 75; % Softer gearbox

LK.MotorEncoder.Counts_rev = -Joint.MotorEncoder.Counts_rev;

LK.OutputEncoder.Offset = 151500;

LK.SpringEncoder.Offset = 807400;

LK.Limits.MotorAngleMin_rad = 0.0;
LK.Limits.MotorAngleMax_rad = 1.8;
LK.Limits.MaxAcceleration_rad_s2 = 50;
LK.PVALimiter.DampingConstant = 65;

LK.Guards.JointAnglePositionGuardMin_rad = LK.Limits.MotorAngleMin_rad - 0.05;
LK.Guards.JointAnglePositionGuardMax_rad = LK.Limits.MotorAngleMax_rad + 0.05;

LK.TorqueControllerType = 3; % DOB controller, do not change 
LK.DOBController.SEA_inertia = (0.87262e-4 + 0.112e-4) * LK.GearboxRatio^2 + 5.0e-4;
LK.DOBController.SEA_damper = LK.DOBController.SEA_inertia / LK.DOBController.SEA_tau;
LK.DOBController.System_Zeta = 1.0;
LK.DOBController.DOB_Zeta = LK.DOBController.System_Zeta;
LK.DOBController.DOBGain = 0.65;

LK.AdaptiveMaxMotorTorque.MaxMotorTorqueAtZeroVelocity_Nm = abs(LK.Motor.MaxCurrent_A * LK.Motor.MotorConstant_Nm_A * LK.GearboxRatio);
LK.AdaptiveMaxMotorTorque.MaxMotorTorqueAtMaxVelocity_Nm = 0.5 * LK.AdaptiveMaxMotorTorque.MaxMotorTorqueAtZeroVelocity_Nm;

%% Left Ankle

LA = Joint;

LA.OutputEncoder.Offset = 475050;

LA.SpringEncoder.Offset = 152150;

LA.Limits.MotorAngleMin_rad = -0.85;
LA.Limits.MotorAngleMax_rad = 0.35;
LA.Limits.MaxAcceleration_rad_s2 = 50;
LA.PVALimiter.DampingConstant = 100;

LA.Guards.JointAnglePositionGuardMin_rad = LA.Limits.MotorAngleMin_rad - 0.05;
LA.Guards.JointAnglePositionGuardMax_rad = LA.Limits.MotorAngleMax_rad + 0.05;

LA.DOBController.System_Zeta = 1;
LA.DOBController.DOB_Zeta = LA.DOBController.System_Zeta;
LA.DOBController.DOBGain = 0.65;

%% Right Hip Abduction

RHA = Joint;

RHA.Motor.MotorConstant_Nm_A = -Joint.Motor.MotorConstant_Nm_A;

RHA.MotorEncoder.Counts_rev = -Joint.MotorEncoder.Counts_rev;

RHA.OutputEncoder.Counts_rev = -Joint.OutputEncoder.Counts_rev;
RHA.OutputEncoder.Offset = 153816;
RHA.OutputEncoder.ROMIncludesZero = 1;

RHA.SpringEncoder.Counts_rev = -Joint.SpringEncoder.Counts_rev;
RHA.SpringEncoder.Offset = 24040;
RHA.SpringEncoder.ROMIncludesZero = 1;

RHA.Limits.MotorAngleMin_rad = -0.25;
RHA.Limits.MotorAngleMax_rad = 0.25;

RHA.Guards.JointAnglePositionGuardMin_rad = RHA.Limits.MotorAngleMin_rad - 0.05;
RHA.Guards.JointAnglePositionGuardMax_rad = RHA.Limits.MotorAngleMax_rad + 0.05;

%% Right Hip Flexion

RHF = Joint;

RHF.Motor.MotorConstant_Nm_A = -Joint.Motor.MotorConstant_Nm_A;

RHF.MotorEncoder.Counts_rev = -Joint.MotorEncoder.Counts_rev;

RHF.OutputEncoder.Counts_rev = -Joint.OutputEncoder.Counts_rev;
RHF.OutputEncoder.Offset = 372695;
RHF.OutputEncoder.ROMIncludesZero = 1;

RHF.SpringEncoder.Counts_rev = -Joint.SpringEncoder.Counts_rev;
RHF.SpringEncoder.Offset = 221450;

RHF.Limits.MotorAngleMin_rad = -0.2;
RHF.Limits.MotorAngleMax_rad = 2.4;

RHF.Guards.JointAnglePositionGuardMin_rad = RHF.Limits.MotorAngleMin_rad - 0.05;
RHF.Guards.JointAnglePositionGuardMax_rad = RHF.Limits.MotorAngleMax_rad + 0.05;

%% Right Knee

RK = Joint;

RK.Motor.MotorConstant_Nm_A = -Joint.Motor.MotorConstant_Nm_A;
RK.GearboxRatio = 75; % Softer gearbox

RK.MotorEncoder.Counts_rev = -Joint.MotorEncoder.Counts_rev; 

RK.OutputEncoder.Counts_rev = -Joint.OutputEncoder.Counts_rev;
RK.OutputEncoder.Offset = 958840;

RK.SpringEncoder.Counts_rev = -Joint.SpringEncoder.Counts_rev;
RK.SpringEncoder.Offset = 742065;

RK.Limits.MotorAngleMin_rad = 0.0;
RK.Limits.MotorAngleMax_rad = 1.8;
RK.Limits.MaxAcceleration_rad_s2 = 50;
RK.PVALimiter.DampingConstant = 65;

RK.Guards.JointAnglePositionGuardMin_rad = RK.Limits.MotorAngleMin_rad - 0.05;
RK.Guards.JointAnglePositionGuardMax_rad = RK.Limits.MotorAngleMax_rad + 0.05;

RK.DOBController.SEA_inertia = (0.87262e-4 + 0.112e-4) * RK.GearboxRatio^2 + 5.0e-4;
RK.DOBController.SEA_damper = RK.DOBController.SEA_inertia / RK.DOBController.SEA_tau;
RK.DOBController.System_Zeta = 1;
RK.DOBController.DOB_Zeta = RK.DOBController.System_Zeta;
RK.DOBController.DOBGain = 0.65;

RK.AdaptiveMaxMotorTorque.MaxMotorTorqueAtZeroVelocity_Nm = abs(RK.Motor.MaxCurrent_A * RK.Motor.MotorConstant_Nm_A * RK.GearboxRatio);
RK.AdaptiveMaxMotorTorque.MaxMotorTorqueAtMaxVelocity_Nm = 0.5 * RK.AdaptiveMaxMotorTorque.MaxMotorTorqueAtZeroVelocity_Nm;


%% Right Ankle

RA = Joint;

RA.Motor.MotorConstant_Nm_A = -Joint.Motor.MotorConstant_Nm_A;

RA.MotorEncoder.Counts_rev = -Joint.MotorEncoder.Counts_rev;

RA.OutputEncoder.Counts_rev = -Joint.OutputEncoder.Counts_rev;
RA.OutputEncoder.Offset = 769410;

RA.SpringEncoder.Counts_rev = -Joint.SpringEncoder.Counts_rev;
RA.SpringEncoder.Offset = 89190;
RA.SpringEncoder.ROMIncludesZero = 1;

RA.Limits.MotorAngleMin_rad = -0.85;
RA.Limits.MotorAngleMax_rad = 0.35;
RA.Limits.MaxAcceleration_rad_s2 = 50;
RA.PVALimiter.DampingConstant = 100;

RA.Guards.JointAnglePositionGuardMin_rad = RA.Limits.MotorAngleMin_rad - 0.05;
RA.Guards.JointAnglePositionGuardMax_rad = RA.Limits.MotorAngleMax_rad + 0.05;

RA.DOBController.System_Zeta = 1;
RA.DOBController.DOB_Zeta = RA.DOBController.System_Zeta;
RA.DOBController.DOBGain = 0.65;

%% Combine

Joint_params = struct;
Joint_params.LHA = LHA;
Joint_params.LHF = LHF;
Joint_params.LK = LK;
Joint_params.LA = LA;
Joint_params.RHA = RHA;
Joint_params.RHF = RHF;
Joint_params.RK = RK;
Joint_params.RA = RA;

clear Joint LHA LHF LK LA RHA RHF RK RA
