clc;

if ~exist('file', 'var')
    [filename, pathname] = uigetfile('*.mat', 'Select dataset');
    file = fullfile(pathname, filename);
end

dataset = makeDataset(file);

data = makeTableFromDataset(dataset);
