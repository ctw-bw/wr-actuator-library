function errors = getJointErrorString(codes, with_hex)
%GETJOINTERRORSTRING Get error description from code
%   errors = getJointErrorString()
%   errors = getJointErrorString(1234)
%   errors = getJointErrorString([1234; 99; 0])
%   errors = getJointErrorString([1234; 99; 0], true)
%
%   Inputs:
%       codes (optional):   Either a single error code or an array of codes
%       with_hex (optional):    When `true` the hex code for an error is
%                               included too (defaults to `false`)
%
%   When no input arguments are given the list of error descriptions is 
%   simply returned. These are ordered by bit, i.e. the firs in the list
%   corresponds to the first bit.
%
%   Outputs:
%       errors: A string cell array of error strings, or a cellarray of 
%               string cell arrays of error strings when `codes` is an
%               array too
%
%   Function is anagolous to the older DecodeJointError(), only without
%   relying on the older char arrays.
%   The list of errors is now retrieved from DecodeJointError(). Make 
%   sure this function is in your path too
%
% See also DECODEJOINTERROR.

%% List descriptions

error_descriptions = DecodeJointError();

%% Append hex codes

if nargin < 1
    errors = error_descriptions;  % Simply return the entire list
    return
end

if nargin < 2
    with_hex = false;
end

if with_hex
    for i = 1:length(error_descriptions)
        error_descriptions{i} = sprintf('%s (0x%s)', error_descriptions{i}, dec2hex(i, 8));
    end
end

%% Process codes

codes = abs(codes(:)); % Force as rows, make positive

code_bool = fliplr(dec2bin(codes, 32) == '1'); % LSB first

if length(codes) == 1
    errors = error_descriptions(code_bool);
    return;
end

errors = cell(length(codes), 1);

for i = 1:length(codes)
    
    list = error_descriptions(code_bool(i, :));
    
    errors{i} = list;
    
end

end

