function datasets = makeDatasetsFromFolder(directory, name)
%MAKEDATASETSFROMFOLDER Create a list of dataset structs
%   dataset = makeDatasetsFromFolder(directory, name)
%
%   See `makeDataset` for general purpose.
%   This version will search for all datasets in a given folder, returning
%   a list of dataset structs instead of a single one.
%   Files will be searched like: `%directory%/*%name%*.mat`
%
%   Input:
%       directory:  Directory to search
%       name:       Type of data to search for (e.g. "JointData")
%
%   Output:
%       See `makeDataset`

if nargin < 2
    name = '*';
end

%% Search for files

if ~exist(directory, 'dir')
    error('Directory `%s` cannot be found!', directory);
end

if strcmp(name, '*')
    file_glob = '*.mat';
else
    file_glob = sprintf('*%s*.mat', name);
end

path_glob = fullfile(directory, file_glob);

files_list = dir(path_glob); % Get list of files (cell of structs)

files_matches = regexp({files_list.name}, '\d\d\d\d\d\d_\d\d\d\d\d\d', 'match'); % Match datetime strings

[~, idx_unique] = unique([files_matches{:}]); % Only keep unique values

% Get dataset for the first of each files set
for i = 1:numel(idx_unique)
    idx = idx_unique(i);
    datasets(i) = makeDataset(fullfile(directory, files_list(idx).name));
end
% There is no convenient way to pre-allocate the array of structs beforehand

end
