function events = getWindowsEventsFromDataset(dataset)
%GETWINDOWSEVENTSFROMDATASET Create table of windows events
%   Use the logged events file to create overview. The events file needs to
%   be listed in the dataset (happens automatically if it exists).
%
%   Inputs:
%       dataset: Dataset wrapper created by makeDataset()
%
%   Outputs:
%       events: Table of events
%
%   Returned value is `false` if no events log was present. An empty table
%   means the log was found but there are no events.

%% Check file
if ~dataset.windowsevents_file
    events = false;
    return
end

%% Do import (generated with MATLAB import data wizard

file_handle = fopen(dataset.windowsevents_file, 'r');

data_array = textscan(file_handle, '%s%s%s%f%s%[^\n\r]', inf, ...
    'Delimiter', '\t', 'TextType', 'string', ...
    'ReturnOnError', false, 'EndOfLine', '\r\n');

fclose(file_handle);

% Convert to cell array
data_array(4) = cellfun(@(x) num2cell(x), data_array(4), 'UniformOutput', false);
data_array([1, 2, 3, 5]) = cellfun(@(x) cellstr(x), data_array([1, 2, 3, 5]), 'UniformOutput', false);
events_array = [data_array{1:end-1}];

%% Make table
events = cell2table(events_array, 'VariableNames', {'timestamp', 'type', 'provider', 'code', 'description'});

if height(events) > 0
    events.timestamp = datetime(events.timestamp, ...
        'InputFormat', 'yyy-MM-dd''T''HH:mm:ss.SSS', ...
        'Format', 'dd-MMM-yyyy HH:mm:ss.SSSS');
end

end
