function printEvents(events, varargin)
%PRINTEVENTS Print events table in readable format
%
%   Inputs:
%       events: Table events
%
%   Options:
%       'ToFile':   Set filename to save the result to (default: false)
%       'Format':   Determines how the results
%                   are shown (default: 'table')
%                   'table' - Show as pretty table (incl. whitespace)
%                   'list' - Show as list (excl. whitespace)
%                   'csv' - Save as CSV (CLI display is still like 'table')

%% Return if empty
if isempty(events)
    fprintf('No events in dataset\n');
    return;
end

%% Arguments

args = inputParser();  % Use built-in parser
addOptional(args, 'ToFile', false);
addOptional(args, 'Format', 'table');
parse(args, varargin{:});

%% Make pretty print

fprintf('The absolute datetime of errors and states is not always accurate! The datetimes of Windows events are.\n');

events.time = cellstr(num2str(events.time, '%.3f')); % Round time to ms
events.timestamp = cellstr(events.timestamp, 'yyyy-MM-dd HH:mm:ss.SSS');

file_str = '';
  
if strcmpi(args.Results.Format, 'list')
    
    file_str = 'Time [s] - Datetime - Event';
    for i = 1:height(events)
       file_str = sprintf('%s%s - %s - %s - %s\n', file_str, events.time{i}, ...
            events.timestamp{i}, events.event{i});
    end
    
    fprintf(file_str);
    
else  % No special prints configured
    
    disp(events);  % Use built-in disp() to print the table
    
end

%% Save to file

if args.Results.ToFile
    if strcmpi(args.Results.Format, 'csv')
        
        writetable(events, args.Results.ToFile, 'WriteRowNames', true, ...
            'Delimiter', ',', 'QuoteStrings', true);
        
    elseif strcmpi(args.Results.Format, 'table')
        
        file_str = evalc('disp(events)');
        file_str = erase(file_str, '<strong>');
        file_str = erase(file_str, '</strong>');
        fid = fopen(args.Results.ToFile, 'w');
        fprintf(fid, '%s', file_str);
        fclose(fid);
        
    elseif strcmpi(args.Results.Format, 'list')
        
        fid = fopen(args.Results.ToFile, 'w');
        fprintf(fid, '%s', file_str);
        fclose(fid);
        
    end
    
end