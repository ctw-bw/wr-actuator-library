function printErrors(errors, varargin)
%PRINTERRORS Print errors table in readable format
%   Show table with decoded drive errors into understandable strings.
%   Errors are listed per row, with each row listing a joint.
%
%   Inputs:
%       errors: Errors table
%
%   Options:
%       'ToFile':   Set filename to save the result to (default: false)
%       'Format':   Determines how the results
%                   are shown (default: 'table')
%                   'table' - Show as pretty table (incl. whitespace)
%                   'list' - Show as list (excl. whitespace)
%                   'csv' - Save as CSV (CLI display is still like 'table')
%       'Timestamp':    Include the absolute timestamp too (default: true)

%% Return if empty
if isempty(errors)
    fprintf('No errors detected in dataset\n');
    return;
end

%% Arguments

args = inputParser();  % Use built-in parser
addOptional(args, 'ToFile', false);
addOptional(args, 'Format', 'table');
addOptional(args, 'Timestamp', true);
parse(args, varargin{:});

%% Create summary first

errors = getErrorsSummary(errors);

%% Make pretty print

% MATLAB never passes by reference, we can safely modify the `errors`
% variable here
errors.time = cellstr(num2str(errors.time, '%.3f')); % Round time to ms

if ismember('timestamp', errors.Properties.VariableNames) && ~args.Results.Timestamp
    errors.timestamp = []; % Remove column when presetn but not requested
end

file_str = '';
  
if strcmpi(args.Results.Format, 'list')
    
    if ismember('timestamp', errors)
        file_str = 'Datetime (Time [s]) - Joints - Hex code - Descriptions';
        for i = 1:height(errors)
            file_str = sprintf('%s%s (%s) - %s - %s - %s\n', ...
                file_str, errors,datetime, errors.time{i}, ...
                errors.Joints{i}, errors.ErrorHex{i}, errors.ErrorDescriptons{i});
        end
        
    else
        file_str = 'Time [s] - Joints - Hex code - Descriptions';
        for i = 1:height(errors)
            file_str = sprintf('%s%s - %s - %s - %s\n', file_str, errors.time{i}, ...
                errors.Joints{i}, errors.ErrorHex{i}, errors.ErrorDescriptons{i});
        end
    end
    
    fprintf(file_str);
    
else  % No special prints configured
    
    disp(errors);  % Use built-in disp() to print the table
    
end

%% Save to file

if args.Results.ToFile
    if strcmpi(args.Results.Format, 'csv')
        
        writetable(errors, args.Results.ToFile, 'WriteRowNames', true, ...
            'Delimiter', ',', 'QuoteStrings', true);
        
    elseif strcmpi(args.Results.Format, 'table')
        
        file_str = evalc('disp(errors)');
        file_str = erase(file_str, '<strong>');
        file_str = erase(file_str, '</strong>');
        fid = fopen(args.Results.ToFile, 'w');
        fprintf(fid, '%s', file_str);
        fclose(fid);
        
    elseif strcmpi(args.Results.Format, 'list')
        
        fid = fopen(args.Results.ToFile, 'w');
        fprintf(fid, '%s', file_str);
        fclose(fid);
        
    end
    
end