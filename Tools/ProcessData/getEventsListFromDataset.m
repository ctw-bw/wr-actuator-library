function events = getEventsListFromDataset(dataset, varargin)
%GETEVENTSLISTFROMDATASET Get neat list of events in dataset
%   events = getEventsListFromDataset(dataset)
%
%   Inputs:
%       dataset: Dataset wrapper produced by makeDataset()
%
%   Options:
%       'FatalOnly':    See other methods (default: false)
%       'SkipFirst':    See other methods (default: 5)
%       'Verbose':      See other methods (default: true)
%
%   Outputs:
%       events: Table of interesting events
%
%   Events are generated from errors, the drives being enabled/disabled and
%   saved Windows events.
%
%   The returned table has the columns:
%       `time`:         Time in seconds
%       `timestamp`:    System datetime
%       `event`:        Description of the event
%
%   See also GETERRORSFROMDATASET.

%% Process arguments
args = inputParser();   % Use built-in parser
addOptional(args, 'FatalOnly', false);
addOptional(args, 'SkipFirst', 5);
addOptional(args, 'Verbose', true);
parse(args, varargin{:});

% events_list = cell(0,4);
events = array2table(zeros(0, 4), 'VariableNames', {'time', 'timestamp', 'type', 'event'});
events.timestamp = NaT([0,1]);  % Force to datetime type to allow concatenation

%% Get enable/disable events

data_drives = makeTableFromDataset(dataset, ...
    'Fields', {'time', 'timestamp', '*_OperationEnabled'}, ...
    'Verbose', args.Results.Verbose);

enabled = all(table2array(data_drives(:, 3:end)), 2); % Merge drives together
toggle_idx_shifted = (enabled(2:end) ~= enabled(1:end-1));  % Search state changes
toggle_idx = [false; toggle_idx_shifted];  % Ignore first point

states = enabled(toggle_idx);

if ~isempty(states)
    
    s = size(states);
    
    states_events = table();  % Order of columns matters
    states_events.time = data_drives.time(toggle_idx);
    states_events.timestamp = data_drives.timestamp(toggle_idx);
    states_events.type = repmat({'State'}, s);
    
    states_events.event = cell(size(states));
    states_events.event(states) = {'Drives enabled'};
    states_events.event(~states) = {'Drives disabled'};
    
    events = [events; states_events];
end

%% Get errors

errors = getErrorsFromDataset(dataset, ...
    'FatalOnly', args.Results.FatalOnly, ...
    'SkipFirst', args.Results.SkipFirst, ...
    'Verbose', args.Results.Verbose); % Copying the arguments seems the only way

if ~isempty(errors)
    
    errors_summary = getErrorsSummary(errors);
    
    s = [height(errors_summary), 1];
    
    error_events = table();  % Order of columns is relevant
    error_events.time = errors_summary.time;
    error_events.timestamp = NaT(s);
    error_events.type = repmat({'Error'}, s);
    error_events.event = strcat(errors_summary.Joints, {': '}, ...
        errors_summary.ErrorDescriptons, {' ('}, errors_summary.ErrorHex, {')'});
    
    events = [events; error_events];
    
end

%% Get Windows Events

wevents_raw = getWindowsEventsFromDataset(dataset);
if istable(wevents_raw) && ~isempty(wevents_raw)
    
    s = [height(wevents_raw), 1];
    
    wevents = table();  % Order of columns is relevant
    wevents.time = nan(s);
    wevents.timestamp = wevents_raw.timestamp;
    wevents.type = repmat({'Windows Event'}, s);
    wevents.event = wevents_raw.description;
    
    events = [events; wevents];
end


%% Tune output - Create `time` from `timestamp` and vice-versa

idx_nan = isnan(events.time);  % Fill in time based on datetime
events.time(idx_nan) = seconds(events.timestamp(idx_nan) - dataset.datetime);

idx_nat = isnat(events.timestamp);  % Fill in datetime based on time
events.timestamp(idx_nat) = dataset.datetime + seconds(events.time(idx_nat));

events = sortrows(events, 'time');  % Sort

end
