# Data Processing

Data from the WE2 is logged as chunked .mat files. With the tools in this folder these can be loaded.

See https://et-be.atlassian.net/wiki/spaces/SP/pages/15774464/Data+and+Error+Processing for more information about data and error processing.

## Dataset

A set of chunks belonging to one sequence (one trial) is one dataset. Use `makeDataset` to find the related files and use `makeTableFromDataset` to actually load the data from a dataset.

Check out the doc headers of the MATLAB files for more information, e.g. `doc makeDataset`

## Examples

An basic example of a complete analysis:

```
path = 'C:\path\log\201002_120329_WE2_JointData_part0.mat' % Suffix is handled automatically

dataset = makeDataset(path);

events = getEventsListFromDataset(dataset);

printEvents(events);
```

You can easily do more in-depth analysis:

```
dataset = makeDataset('C:\path\to\file\200901_112741_WE2_JointData');

% Get the time column and everything related to the Left Knee (LK)
data = makeTableFromDataset(dataset, 'Fields', {'time', 'LK_*'});

errors = getErrorsFromDataset(dataset);

error_time = errors.time(1); % Time of the first error

% Get only the rows in `data` within 0.03 s of the error time
data_section = data(data.time > error_time - 0.02 & data.time < error_time + 0.01, :);

% Plot desired columns
scatter(data.time, data.LK_JointAngle_rad, '.');
% ...
```
 