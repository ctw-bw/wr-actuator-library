function data_table = makeTableFromDataset(dataset, varargin)
%MAKETABLEFROMDATASET Make table object from dataset
%   data_table = makeTableFromDataset(dataset)
%   data_table = makeTableFromDataset(dataset, 'Fields', {'time', '*Error'})
%   data_table = makeTableFromDataset(dataset, 'Verbose', true)
%
%   Inputs:
%       dataset:    Wrapper struct from makeDataset()
%   Options:
%       'Fields':   A subset of fields to only include, the '*' wildcard
%                   is allowed (all fields are loaded when omitted)
%       'Verbose':  Print progress of loading files (default `true`)
%
%   Outputs:
%       data_table: Table object
%
%   If the `timestamp` field is included, it will already be converted to a
%   datetime object with correct timezone. If the timestamp was included
%   but is not present or invalid in the data (e.g. because an older data
%   file is used) the column will be filled using the start datetime and
%   time column (a warning will be shown too).

%% Process arguments
args = inputParser();   % Use built-in parser
addOptional(args, 'Fields', {});
addOptional(args, 'Verbose', true);
parse(args, varargin{:});

%% Perform field filtering
if isempty(args.Results.Fields)
    fields = dataset.field_names;
else
    patterns = args.Results.Fields;
    regex_matches = cell(numel(patterns), numel(dataset.field_names));
    for i = 1:numel(patterns)
        regex = ['^' regexptranslate('wildcard', patterns{i}) '$'];
        regex_matches(i, :) = regexpi(dataset.field_names, regex, 'match');
    end
    fields_idx = any(~cellfun(@isempty, regex_matches), 1);  % Indices of selected fields from complete set
    fields = dataset.field_names(fields_idx);
end

%% Iteratively load data

data_matrix = [];  % Create empty matrix and append each file

n_fields = length(dataset.field_names); % Total number of fields in dataset

for i = 1:length(dataset.files)
    
    if args.Results.Verbose
        fprintf('Loading file %d out of %d...\n', i, length(dataset.files));
    end
    
    try
        set_struct = load(fullfile(dataset.folder, dataset.files{i}));
    catch err
        
        if strcmpi(err.identifier, 'MATLAB:load:unableToReadMatFile')
            warning('File `%s` could not be loaded and might be corrupt! Skipping this file for now', dataset.files{i});
            continue;  % Skip this file
        end
            
        rethrow(err);  % Do not catch all possible errors
    end
    
    set_fields = fieldnames(set_struct); % `load()` outputs a struct of variables
    set_array = set_struct.(set_fields{1}); % Take the first variable from the file (should be only one anyway)
    
    if size(set_array, 1) ~= n_fields && size(set_array, 2) ~= n_fields
        % Neither ncols or nrows matches with number of fields
        error('Data file has different number of columns (%d) then described by the signal names (%d)', ...
            min(size(set_array)), n_fields);
        
    elseif size(set_array, 1) == n_fields && size(set_array, 2) ~= n_fields
        set_array = transpose(set_array); % Take transpose if array is horizontal
    end
    
    if length(fields) ~= n_fields
        set_array = set_array(:, fields_idx); % Crop to the selected fields
    end
    
    data_matrix = [data_matrix; set_array];
end

data_table = array2table(data_matrix, 'VariableNames', fields); % Turn big matrix into table

if args.Results.Verbose
    fprintf('Loaded %d files\n\n', length(dataset.files));
end

%% Make datetime objects

format = 'dd-MMM-yyyy HH:mm:ss.SSSS';

timestamp_quality = 0.0;

if ismember('timestamp', data_table.Properties.VariableNames)
    
    data_table.timestamp(data_table.timestamp < 0) = 0;  % First ~50 samples are negative for some reason
    
    % Replace stamp by datetime object
    data_table.timestamp = datetime(data_table.timestamp, ...
        'ConvertFrom', 'epochtime', 'Format', format, ...
        'TimeZone', 'UTC');
    
    % Check if the datetimes are valid
    timestamp_quality = sum(data_table.timestamp > datetime('2000-01-01 00:00', 'TimeZone', 'UTC')) / height(data_table);
end

if ~ismember('timestamp', fields) || timestamp_quality < 0.5
    % timestamp was selected but it does not exist in the data
    % Or a timestamp did exist but contained too much invalid data (e.g.
    % TwinCAT solution was not set up correctly)
    % Data could be from before wall-time was included
    
    % Build timestamp from start datetime
    data_table.timestamp = dataset.datetime + seconds(data_table.time);
    data_table.timestamp.Format = format;
    
    warning('Timestamp in dataset %s is composed from start-time instead of absolute time.\nThe result is not accurate.', datestr(dataset.datetime));
    
    % Move last column to the second position
    data_table = [data_table(:, 1), data_table(:, end), data_table(:, 2:end-1)];
end

if ismember('timestamp', data_table.Properties.VariableNames)
    data_table.timestamp.TimeZone = 'Europe/Amsterdam'; % Convert to our timezone (needed for correct display)
end

end
