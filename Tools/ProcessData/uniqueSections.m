function idx = uniqueSections(list, type)
%UNIQUESECTIONS Find repeated values as sections
%   Find only the first (or last) indicies of blocks.
%
%   Inputs:
%       list: Array to search
%
%   Outputs:
%       idx:    Logical array where each 1 represents a new block
%       type:   Either 'first' or 'last', to find the first or last index
%               of a bock (default 'first')
%
%   Example:
%   
%   >> list = [9 9 9 9 5 3 3 3 3 3 3 7 7 9 9 9];
%   >> idx_start = find(list > 5 & uniqueSections(list))
%
%       idx_start = 
%
%            1 12 14
%
%   See also FIND.

% Force to column vector
list = list(:);

if nargin < 2 || strcmpi(type, 'first')
    idx = ([1; diff(list)] ~= 0); % First value is always interesting, so make it non-zero
    return;
end

% To get the last block element instead of the first we need to flip the
% list
list = flip(list);

idx = ([flip(diff(list)); 1] ~= 0); % Now the last value is always interesting

end

