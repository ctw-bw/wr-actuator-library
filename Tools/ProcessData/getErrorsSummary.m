function errors_summary = getErrorsSummary(errors)
%GETERRORSSUMMARY Get summary from errors table
%   errors = getErrorsSummary(errors_table)
%
%   Inputs:
%       errors_table: Table produced by getErrorsFromDataset()
%
%   Outputs:
%       errors: Shorter errors table
%
%   This method summarizes the errors table combining identical erorrs to
%   several joints at the same time instance.
%   Additionally it adds descriptions to the errors.

%% Merge errors for the same joint at the same time

original_joints = errors.joint;

[~, idx_merged, idx_original] = unique([errors.time, errors.error_code], 'stable', 'rows');

errors = errors(idx_merged, :);  % Keep only the unique combinations

errors.joint = []; % Remove original column

joints_groups = cell(height(errors), 1);

for i = 1:height(errors)
    
    joints_idx = (idx_original == i);           % List of orignal rows which were merged into one
    joints = original_joints(joints_idx);       % Joints which were merged
    joints_groups{i} = strjoin(joints, ', ');   % Join these joints into one string
    
end

%% Add hex column

hex_strings = cellstr(dec2hex(errors.error_code, 8));
error_hex = cellfun(@(x) sprintf('0x%s', x), hex_strings, 'UniformOutput', false);

%% Add error descriptions column

error_strings = getJointErrorString(errors.error_code, false); % Without hex

if ~isempty(error_strings) && ~iscell(error_strings{1})
    % Downside of flexible functions is that when there is a single error
    % it is read as scalar, so here we force it back to an array of arrays.
    error_strings = {error_strings};
end

error_strings_conc = cellfun(@(x) strjoin(x, ', '), error_strings, 'UniformOutput', false);

errors.error_code = [];  % Remove decimal code column

%% Add new pretty columns and print

errors_summary = [...
    errors, ...
    cell2table(joints_groups, 'VariableNames', {'Joints'}), ...
    cell2table(error_hex, 'VariableNames', {'ErrorHex'}), ...
    cell2table(error_strings_conc, 'VariableNames', {'ErrorDescriptons'})...
    ];

end

