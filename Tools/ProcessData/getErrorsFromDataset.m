function error_table = getErrorsFromDataset(dataset, varargin)
%GETERRORSFROMDATASET Find fatal drive errors in a dataset
%   error_list = getErrors(dataset)
%
%   Inputs:
%       dataset:    Data wrapper from makeDataset()
%
%   Options:
%       'FatalOnly':    Only show the errors that cause the drive to
%                       shut down (default: true)
%       'SkipFirst':    Skip first n seconds (default: 5), useful to ignore
%                       initial boot errors
%
%   Outputs:
%       error_list: Table of errors
%
%   Fatal errors are extracted from when the drives went from enabled to
%   disabled:
%
%   Enabled     |   Error
%       1       |   0
%       1       |   256             <--- This error code is extracted
%       0       |   256
%
%   This assumes the error is registered in one frame and the drives
%   disabled in the next one.
%
%   When all errors are requested, they are provided non-repeating. E.g. if
%   an error is reported for 100 consequetive cycles, it will only be
%   mentioned the first time.

%% Process arguments
args = inputParser();   % Use built-in parser
addOptional(args, 'FatalOnly', true);
addOptional(args, 'SkipFirst', 5);
addOptional(args, 'Verbose', true);
parse(args, varargin{:});

%% Load data
data = makeTableFromDataset(dataset, ...
    'Fields', {'time', 'timestamp', '*OperationEnabled', '*JointError'}, ...
    'Verbose', args.Results.Verbose);

error_array = cell(0, 4);

%% Find and append errors

for i_joint = 1:length(dataset.joints)
    
    joint_name = dataset.joints{i_joint};
    
    list_errors = data.(sprintf('%s_JointError', joint_name));
    
    if args.Results.FatalOnly  % Get errors that made the drive disable
        list_enabled = data.(sprintf('%s_OperationEnabled', joint_name));
        
        % Get indices where enabled went from 1 to 0
        idx_halted_shifted = (list_enabled(1:end-1) == 1 ...
            & list_enabled(2:end) == 0 ...
            & list_errors(2:end) ~= 0);
        idx_errors = [false; idx_halted_shifted]; % Indices belonged to shortened array, correct for it
    
    else  % Get all errors (but without them repeating)
        idx_errors = list_errors > 0 & uniqueSections(list_errors);
    end
    
    if args.Results.SkipFirst
        idx_errors = idx_errors & (data.time > args.Results.SkipFirst);
    end
    
    for idx = find(idx_errors)'
        
        new_row = {data.time(idx), data.timestamp(idx), joint_name, list_errors(idx)};
    
        % Ugly array resizing, haven't found a way to optimize
        error_array = [error_array; new_row];
        
    end
    
end

%% Create table, sort by time

error_table = cell2table(error_array, 'VariableNames', {'time', 'timestamp', 'joint', 'error_code'});

error_table = sortrows(error_table, 'time');

end

