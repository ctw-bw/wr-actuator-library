function dataset = makeDataset(path)
%MAKEDATASET  Create a struct with meta information around a files set
%   dataset = makeDataset(path) Create meta wrapper for files
%
%   This fuction creates a wrapper around a set of data files belonging
%   to the same series. This wrapper can then be passed around instead
%   of the actual data.
%   The combined data can be multiple gigabytes, so we avoid loading it
%   into memory in its entirety
%
%   Input:
%       path:   Path to one of the files of a dataset (use absolute path
%               when outside the current directory)
%
%   The .mat extension and '_partXX' suffix will be automatically deleted
%   if present in the path.
%
%   Output:
%       dataset.folder:     Directory where files were found
%       dataset.files:      Ordered list of involved files (files only)
%       dataset.field_names:    List of column names
%       dataset.datetime:   Date and time of start of trial
%       dataset.windowsevents_file: Path to file containing Windows events
%
%   The field_names (signal names) are first attempted to be retrieved from
%   an adjacent signal names files. If no such file is present the names
%   are taken from the /Libraries/ directory. You can always override the
%   fieldnames after running this function like e.g.:
%       dataset.field_names = importSignalnamesFromFile('my_names.txt');

dataset = struct();

%% Search for files and index them

if endsWith(path, '.mat')
    path = path(1:end-4);  % Remove extension if provided
end

parts = regexp(path, '_part[\d]+', 'split');
if numel(parts) > 1
    path = parts{1};  % Only take section before `_partXXX`
end

files = dir(sprintf('%s*', path));  % Get all files matching the prefix (including e.g. _corrupt files)

files_matches = regexpi({files.name}, '(.*)_part(\d+).mat', 'match'); % Use the power of regex to narrow it down further

files = files(~cellfun(@isempty, files_matches));  % Only keep the files that match exactly

if isempty(files)
    error('No data files found at `%s`', path);
end

dataset.folder = files(1).folder; % Just take the first file

files_parts = split({files.name}, {'_part', '.'});  % Split into three parts
if ndims(files_parts) > 2   % If there is a only a single file this variable won't have a third dimension
    files_numbers_str = squeeze(files_parts(:,:,2));    % Take only the middle parts
else
    files_numbers_str = files_parts(2);    % Take only the middle parts
end
files_numbers = str2double(files_numbers_str);      % Turn into numbers (order is still the same as `files`
[~, idx_files] = sort(files_numbers);

dataset.files = {files(idx_files).name}; % Sort to right order and store as cellarray

%% Get datetime from file name

datetime_match = regexp(dataset.files{1}, '(\d\d\d\d\d\d)_(\d\d\d\d\d\d)', 'match');

if isempty(datetime_match)
    dataset.datetime = datetime(2000, 1, 1, 0, 0, 0);  % No datetime found
else
    dataset.datetime = datetime(datetime_match{1}, 'InputFormat', 'yyMMdd_HHmmss');
end

%% Search for Windows Events log

events_file = sprintf('%s_WindowsEvents.txt', dataset.files{1}(1:13));
events_path = fullfile(dataset.folder, events_file);

if isfile(events_path)
    dataset.windowsevents_file = events_path;
else
    dataset.windowsevents_file = false;  % No events log found
end

%% Get fields

field_names = findFieldNames(path);

field_names = {field_names{:}}; % Force into rows

if ~strcmpi(field_names{1}, 'time') % Case-insensitive string compare
    field_names = ['time', field_names]; % Prepend 'time' if not present
end

dataset.field_names = field_names;

%% Extract joint names

% Use a single field to find the joint names
joints_idx = endsWith(dataset.field_names, 'OperationEnabled');
joint_fields = dataset.field_names(joints_idx);
parts = cellfun(@(x)strsplit(x, '_'), joint_fields, 'UniformOutput', false);
dataset.joints = cellfun(@(x)x{1}, parts, 'UniformOutput', false);

end

function field_names = findFieldNames(path)
% FINDFIELDNAMES Method to find the most appropiate signal names
%   This is a function mostly to enable a convenient early return and
%   prevent many nested if statements.
%
%   Inputs:
%       path: Search path of files, e.g. 'D:\Logs\200901_112741_WE2_JointData'

% Try .txt first
signalnames_path = sprintf('%s_signalnames.txt', path);

if isfile(signalnames_path)
    field_names = importSignalnamesFromFile(signalnames_path);
    return
end

% Then try .mat file
signalnames_path = sprintf('%s_signalnames.mat', path);
    
if isfile(signalnames_path)
    loaded_struct = load(signalnames_path);  % Result is a struct with the variable names
    struct_fields = fieldnames(loaded_struct);
    field_names = loaded_struct.(struct_fields{1}); % Take the first variable from the file (should be only one anyway)
    return
end

% Resort to the default the /Libraries/ directory
[script_dir, ~, ~]  = fileparts(mfilename('fullpath'));
field_names = importSignalnamesFromFile([script_dir '\..\..\Libraries\we2_jointdata_names.txt']);

end
