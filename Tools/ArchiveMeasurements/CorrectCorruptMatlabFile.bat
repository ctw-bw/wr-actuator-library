@echo off

IF "%~1" == "" GOTO HELP
python %~dp0\CorrectCorruptMatlabFile.py %1
goto END

:HELP
echo Usage: drag-drop a .mat file onto this .batch-file.
goto END

:END
pause

rem %~dp0 expands to the directory name of this batch file (see https://stackoverflow.com/a/17064031/2173689)
