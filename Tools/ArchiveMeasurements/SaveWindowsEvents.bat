@echo off
IF "%1"=="" goto HELP

rem THE COMMAND
set TwinCATQuery="TcSysSrv|TcEventLogger|TwinCAT"
set DateQuery="/q:*[System[TimeCreated[@SystemTime>='%~1T00:00:00' and @SystemTime<='%~1T23:59:59']]]"
wevtutil qe Application %DateQuery% /f:text | python %~dp0\ProcessWindowsEvents.py %TwinCATQuery%
goto END

rem THE HELP
:HELP
echo SaveWindowsEvents.bat
echo.
echo This script shows all Windows Events of a single date which are related to TwinCAT.
echo (e.g., starting TwinCAT service, info on when a EtherCAT connection was lost/re-established).
echo.
echo This script uses ProcessWindowsEvents.py for formatting the data and filtering.
echo As an argument you must specify a data in the form of "YYYY-MM-DD". If you fail to do so,
echo the result of running this script is undefined.
echo. 
echo Usage: 
echo   SaveWindowsEvents.bat "2018-03-18" ^> EventLog.txt
echo.
pause

:END