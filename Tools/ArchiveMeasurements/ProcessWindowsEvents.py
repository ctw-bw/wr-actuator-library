# ProcessWindowsEvents.py
"""Process output from wevtutil.exe into readable event list

This script takes as input an exported file (or the output directly) from 
wevtutil.exe, containing Windows Events, including TwinCAT events. It reformats
the events into a tab-separated structure, which can be saved to file and read 
in Excel. Optionally, a regexp can be given as argument that is matched to the
full line (you can regexp almost on every field)

Examples (both give only events happened on 9 November):

  wevtutil qe Application /f:text > EventLog.txt
  type EventLog.txt | python ProcessWindowsEvents.py "2016-11-09" > EventLog.txt

  wevtutil qe Application /f:text | python ProcessWindowsEvents.py "2016-11-09" > EventLog.txt
"""

import sys
import re


class EventProcessor:
    """Class to process Windows Event

    These methods are made into a class to make it easier to use externally.

    Example:
        processor = EventProcessor(sys.stdin)
        for lin in iter(processor):
            ...
    """

    FIELDS = ["Date", "Level", "Source", "Event ID", "Description"]

    def __init__(self, handle):
        """Initialize object with link to file stream"""
        self.handle = handle

        self.is_last = False  # If true, need to stop on the next iteration

    def read_line(self):
        """Read the next line from the handle"""
        line = self.handle.readline().strip()
        return line

    def read_and_assert(self, match):
        """This function reads one line, strips whitespaces and checks if it starts with "match", and
        returns everything after the match."""
        line = self.read_line()

        if not line.startswith(match):
            raise Exception("Expected line starting with keyword %s but got the following line: `%s`" % (match, line))

        return line[len(match):].strip()  # Return the part _after_ the matching substring

    def process_one_event(self):
        """Given file handle, it reads one full event and returns a tab-delimited version of it as a string. This
        assumes that the 'Event[X]:' line was already read. It reads all data for this event from the file handle, and
        also reads the next 'Event[X]:' line."""

        # Store all data in one dict first
        data = {
            'LogName': self.read_and_assert("Log Name:"),
            'Source': self.read_and_assert("Source:"),
            'Date': self.read_and_assert("Date:"),
            'Event ID': self.read_and_assert("Event ID:"),
            'Task': self.read_and_assert("Task:"),
            'Level': self.read_and_assert("Level:"),
            'Opcode': self.read_and_assert("Opcode:"),
            'Keyword': self.read_and_assert("Keyword:"),
            'User': self.read_and_assert("User:"),
            'User Name': self.read_and_assert("User Name:"),
            'Computer': self.read_and_assert("Computer:")
        }
        self.read_and_assert("Description:")  # Description start line below it
        description = []
        i = 0
        is_last = False
        while True:
            try:
                line = self.handle.readline().strip()
            except EOFError:
                # This is not really an error, it's just that this is the last Event
                # in the file. So we can continue for now (and the next event parsing
                # will fail due to an EOF error which we'll catch in the main loop.
                is_last = True
                break

            i += 1
            if i > 100:  # EOF detection doesn't seem to work, cut it short. The empty lines are removed anyway.
                is_last = True
                break

            if line.startswith("Event["):  # Next Event found
                break

            description.append(line)

        data['Description'] = " || ".join(description).replace("\t", "  ").strip(" |")

        # Now store the interesting things
        asstring = "\t".join([data[f] for f in self.FIELDS])
        return asstring, is_last

    def __iter__(self):
        """Make object iterable, we don't need to prepare anything"""

        if self.is_last:
            self.handle.seek(0)  # Back to first byte
            self.is_last = False

        line = self.read_line()  # We need to read the first "Event[0]" line

        if not line or not line.startswith('Event['):
            self.is_last = True  # Empty list, we got nothing to iterate

        return self

    def __next__(self):
        """Each loop when iterating"""

        if self.is_last:
            raise StopIteration  # Stop iterating

        try:
            (line, is_last) = self.process_one_event()
        except EOFError:
            raise StopIteration  # Stop iteration

        if is_last:
            self.is_last = True

        return line


if __name__ == '__main__':

    # Check if we have data on stdin, otherwise, show help
    if sys.stdin.isatty() or len(sys.argv) > 2:
        # In this case, stdin is getting data directly from the keyboard during the
        # program execution. This is not what we want, so display help and exit.
        print(__doc__)
        exit(1)

    if len(sys.argv) == 2:
        regexp = re.compile(sys.argv[1])
    else:
        regexp = None

    processor = EventProcessor(sys.stdin)

    print("\t".join(processor.FIELDS))  # Print header

    for line_i in processor:

        if regexp is None:
            # No filtering
            print(line_i)
        else:
            # Filtering
            if regexp.search(line_i) is not None:
                print(line_i)
