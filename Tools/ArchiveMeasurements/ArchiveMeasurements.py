# ArchiveMeasurements.py

from glob import glob
import os
import os.path
import traceback
import sys
import re
import shutil
import datetime
import struct
import configparser
import time
import scipy.io

import CorrectCorruptMatlabFile  # Contains a function to reconstruct the data size
from ProcessWindowsEvents import EventProcessor  # Used to, well, process windows events

"""This script manages the logged data files, and does the following things:
- Rename all data files so that they start with a timestamp
- (optionally) Move data files to a different directory (e.g., on a USB drive)
- (optionally) Copy SignalNames files with the data files


This script keeps an eye on the given directory(s) and will rename any files that seem to be TwinCAT
measurement files to something with a timestamp. The idea is that, when this is done, data files are
never overwritten and that the data is more easily traceable. The timestamp used is the date_time of
the start of the experiment (when TwinCAT was set to Run mode).

Also, the script can move the data to another directory, e.g., on another drive. The idea here is
that we don't want TwinCAT to write on USB drives (has caused timing problems in the past), but we
do want the data to be stored there (and not on the small internal SSD drive).

The logged data from TwinCAT does not include signal names (only the numerical data). Within the
WE2_experiments repository, there are files describing the signal names. This script can, optionally,
resolve which SignalNames file is related and copy that into the data directory.

This script uses two configuration files, being:
- ArchiveMeasurements.ini
  It holds the following configuration:
  + Which directories to monitor (multiple are possible)
  + Which directory to move the data files to
  This one system-specific and therefore it is not in the repository. 
- ArchiveMeasurementsSignals.ini
  Tells which SignalNames files to copy for specific data files. See the docstring of
  ReadSignalNamesConfigurationFile() for more details. This ini file should be, usually, identical
  for all PC's, therefore it is included in the repository.

INTERNALS (i.e., how it works)
This works by constantly (every 15 seconds) evaluating the whole directory.
If any files are discovered that can be renamed, it does this. A file can be
renamed when:
- The file has been closed for at least 15 seconds  (and thus renamable).
- The file matches the FILE_MASK (regexp)
- The file does NOT mask the ALREADY_RENAMED_FILE_MASK (regexp). This mask should
  match files that were already renamed (otherwise they'll be renamed again).

Internally, the script looks at the creation time of the file and the first time element of the logged
data. With this, it calculates the start time of the experiment.
It then uses the experiment start time for the timestamp (also, it looks at already renamed files;
if there is a timestamp close but not exactly to the calculated one, it uses that timestamp of the
other file. This guarantees that all files belonging to one experiment get exactly the same timestamp).
"""

# Masks for file name matching
FILE_MASK = r'.*part[0-9]+\.mat$'  # regexp
FIRST_FILE_MASK = r'.*part0\.mat$'  # regexp
ALREADY_RENAMED_FILE_MASK = r'^[0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_.*\.mat$'  # regexp


# Print warnings/errors etc
# Separate functions to make it easier to make nice gui later
def logprint(*args, **kwargs):
    """Prints to the log file. The file name of the log file should be stored in a global variable called LOG_FILENAME.
    The file is reopened for each write action; hopefully this limits data loss in case of errors.
    """
    try:
        with open(LOG_FILENAME, "at", newline="\r\n") as f:
            print(*args, file=f, **kwargs)
    except NameError:
        print("No file name for log file known yet -> not logging")
        pass  # Don't log data until we know where to log it
    except FileNotFoundError:
        print("Could not write to log file because the file does not exist\n  (does the directory exist? Is the "
              "external drive there?)")
        pass  # Don't bother for now, maybe the drive will come back and we can continue


def weiprint(*args, indent=0, wei_str=""):
    """Optional argument: indent = n, e.g., wprint("hoi", indent=4)"""
    indentstr = indent * " "

    # Let's try to indent any newlines as well
    def makeindent(v, indentcharacters):
        if isinstance(v, str):
            return v.replace("\n", "\n" + indentcharacters)
        else:
            return v

    args = tuple((makeindent(v, indentstr) for v in args))

    if len(wei_str) != 0:
        wei_str = wei_str + ": "  # append : to it

    print(indentstr + wei_str, *args, sep="")
    logprint(indentstr + wei_str, *args, sep="")


def wprint(*args, **kwargs):
    """Print warning"""
    weiprint(*args, wei_str="WARNING", **kwargs)


def eprint(*args, **kwargs):
    """Print error"""
    weiprint(*args, wei_str="ERROR", **kwargs)


def iprint(*args, **kwargs):
    """Print information"""
    weiprint(*args, wei_str="", **kwargs)


# Configuration/ini-file handling
def read_signal_names_configuration_file():
    """Describes the mapping from data files, to SignalName files. The purpose is to automatically
    copy the correct signal name file with the data so there is, later, no ambiguity on which signals
    are labeled how. See global docstring as well.

    Format: between brackets you place the file name of the data file, without 'partx' or '_partx'.
    Then, as elements, you have keys 'fileN' (N=1, 2, ...), which values point to the appropriate
    SignalName files. This can be eiter an absolute path, or a path relative to the path where
    ArchiveMeasurements.py is located (which is usually more convenient). You can enter more than one
    SignalName file. ArchiveMeasurements.py will automatically pick the one with the correct amount
    of signals. This ensures compatibility and allows for log files with the same name but different
    signals (e.g., WE2_JointData may refer to 1 (Liedtke), 4 (WE1) or 8 (WE2) joints). If multiple
    file are given with the same amount of signals, a warning is shown and the first file is taken.

    Example configuration file for ArchiveMeasurementsSignals.ini:

    [WE2_JointData]
    file1 = ../Libraries/WE2_jointdata_names.mat
    file2 = ../Liedtke/Liedtke_jointdata_names.mat

    [WE2_3D_Outputs]
    file1 = "...."

    We have split the mapping for data file names to signal name files (which hold the names of all
    logged signals) from the normal configuration file. We have done this becasue the SignalName mapping
    is, normally, the same for each system, whereas the normal configuraion file generally differs.
    Hence, the former config file will be in the repository, the second will not.
    """

    def create_wrong_file_key(v):
        """Given a dict v, this function creates a new key "WrongFileX" (where X = 1, 2, ...)"""
        x = 1
        while "WrongFile%i" % x in v.keys():
            x += 1

        return "WrongFile%i" % x

    ini_config = configparser.ConfigParser()
    try:
        with open("ArchiveMeasurementsSignals.ini") as f:
            ini_config.read_file(f)
    except FileNotFoundError:
        wprint("Did not find ArchiveMeasurementsSignals.ini. Copying signalname files is disabled.\n\nPlease have a "
               "look in the source code documentation to see how to setup such a file correctly.\n")

    # Get the absolute path of Archivemeasurements.py
    p = os.path.split(os.path.abspath(sys.argv[0]))[0]

    # We want the configuration as dict of dicts. The 'outer' dict holds the file names for which
    # there are SignalNames files. The inner dicts have as index the number of signal names for each
    # SignalNames file. Also, we want the absolute paths to the files.
    config = {}
    for section in ini_config.sections():
        values = list(ini_config[section].values())  # Files as in config file

        section = section.rstrip("_")  # See docstring of CopySignalNamesFile

        config[section] = {}  # empty dict
        for f in values:
            # Get absolute path
            fa = os.path.realpath(os.path.join(p, f))

            # Find out the number of signals for the file
            try:
                n = get_signal_name_count(fa)  # Returns None if file does not exist
                if n is None:
                    wprint("Could not find SignalNames file %s\n   (expands to %s)" % (f, fa))
            except FileNotFoundError:
                # For any exception, we also use the 'WrongFile' key
                n = None
                wprint("Could not determine number of signals in file %s\n   (expands to %s)" % (f, fa))

            if n is None:
                n = create_wrong_file_key(config[section])

            # Check if the key is already taken (if so, we have two files with the same number of
            # signal names
            if n in config[section].keys():
                wprint("Two SignalNames files with the same number of signals (%i):\n"
                       "   %s\n   %s\n   Taking the first SignalNames file only." %
                       (n, config[section][n], fa))
            else:
                config[section][n] = fa

    return config


def read_configuration_file():
    """If there is an inifile ArchiveMeasurements.ini, it reads it.
    If the file does not exist, it will generate
    the ini-file with settings compatible with WE1 in Rome (so that they don't
    need to do any work).

    Returns a list of directories to be archived.

    Example configuration file ArchiveMeasurements.ini:

    [paths]
    path1 = C:/WE1_logs
    path2 = D:/WE1_logs
    [movetopath]
    path = D:/WE1_logs
    """
    ini_config = configparser.ConfigParser()
    try:
        with open("ArchiveMeasurements.ini") as f:
            ini_config.read_file(f)

    except FileNotFoundError:
        eprint("""No archivemeasurements.ini file found. Please create one (it should be in
        the folder from where you start Python; the easiest is to put it in the we1_experiments/Tools
        folder and run python from there.
        Example (check WE1_logs dirs on C: and D:; moves all renamed log files from C: to D:):
        
            [paths]
            path1 = C:/WE1_logs
            path2 = D:/WE1_logs
            
            [movetopath]
            path = D:/WE1_logs
        """)
        raise  # Re-raise the exception

    if 'paths' not in ini_config.keys():
        raise RuntimeError("Config files is missing the 'paths' section. This is a compulsory one.")

    # The ini-config has a little weird structure; we want something simpler:
    config = {'paths': list(ini_config["paths"].values())}
    if 'movetopath' in ini_config:
        config["movetopath"] = list(ini_config["movetopath"].values())[0]
    else:
        config["movetopath"] = None

    if 'events' in ini_config:
        config["events_query"] = list(ini_config["events"].values())[0]
    else:
        config["events_query"] = False  # Do not log events

    # If we're here, we can parse the configuration (if the ini file didn't exist,
    # a default configuration was made in the Except: part)
    return config


# Helper functions
def is_renamable(filename, min_close_time=15):
    """Checks whether a file is renamable.

    After some testing with TwinCAT, I (Gijs) found out the following:
    - While the file is being written, the modification time and creation time are identical.
    - After writing is done, they differ by the 'duration' of the file; the modification time
      is updatded to the time of closing the file.
    These two can be used to detect if the file is finished. Then, I want that the
    file is closed for at least `minCloseTime` seconds before renaming (in case TwinCAT decides
    to re-open it to write the size into it; I don't know exactly how/when it does
    that, but let's just play safe).
    """
    ctime = os.path.getctime(filename)
    mtime = os.path.getmtime(filename)

    if ctime != mtime and (time.time() - mtime) > min_close_time:
        return True
    else:
        return False


def get_n_rows(filename):
    """Given a .mat file as generatd by TwinCAT, this function returns the number
    of rows (i.e., samples) in it."""
    with open(filename, "rb") as f:
        f.seek(8)
        b = f.read(4)
        n = struct.unpack("<i", b)[0]
    return n


def get_n_signals(filename):
    """Returns the number of signals in the .mat file"""
    with open(filename, "rb") as f:
        f.seek(4)
        b = f.read(4)
        n = struct.unpack("<i", b)[0]
    return n


def get_end_time(filename):
    """Given a .mat file as generated by TwinCAT, returns the time of the last timestamp in the file
    (in seconds)."""

    # First read some data about the file
    with open(filename, "rb") as f:
        f.seek(4)
        b = f.read(4 * 4)
        [n_elements_per_sample, n_samples] = struct.unpack("<iiii", b)

    # Get time of last sample
    if n_samples != 0:
        # The normal case; file was closed correctly and the last sample in the
        # file was fully written (i.e., no half samples were written)
        with open(filename, "rb") as f:
            f.seek(-n_elements_per_sample * 8, 2)  # Seek relative to the end of the file
            b = f.read(8)
            end_time = struct.unpack("<d", b)[0]
    else:
        # Then the file was not closed correctly by TwinCAT and we need to work
        # out the data size ourselves
        r = CorrectCorruptMatlabFile.GetRelevantDataFromMatFile(filename)

        # We don't want to change the file (that would damage the Modification time), just read the
        # data from the correct place
        with open(filename, "rb") as f:
            f.seek(20 + r['varnamelen'] + (r['nSamples'] - 1) * r[
                'nElementsPerSample'] * 8)  # Move pointer to start of last full sample
            b = f.read(8)
            end_time = struct.unpack("<d", b)[0]
    return end_time


def get_start_time(filename):
    """Given a .mat file as generated by TwinCAT, returns the time of the first timestamp in the file
    (in seconds)."""

    # First read some data about the file
    with open(filename, "rb") as f:
        f.seek(4)
        b = f.read(4 * 4)
        [_n_elements_per_sample, _n_samples, _dummy, variable_name_length] = struct.unpack("<iiii", b)

        # Get the time of the first sample
        f.seek(variable_name_length, 1)  # Relative to current position
        b = f.read(8)
        start_time = struct.unpack("<d", b)[0]

    return start_time


def get_time_of_start_of_experiment(filename):
    """Returns a timestamp (numerical, no string), containing the approximate start of the experiment.
    It is determined by checking the creation time of the file (this is the first time it was
    written by the TcExtendedFileWriter) and subtracting the StartTime (see GetStartTime()) from it.
    Note that there may be a few seconds difference in this for each subsequent file."""
    t = os.path.getctime(filename)
    return t - get_start_time(filename)


def get_time_span(filename):
    """Given a .mat file as generated by TwinCAT, this function returns the time difference
    in seconds between the first and last sample in the file. This can be done because
    TwinCAT will always insert the time (in seconds) as the first element of each sample."""

    # First read some data about the file
    with open(filename, "rb") as f:
        f.seek(4)
        b = f.read(4 * 4)
        [n_elements_per_sample, n_samples, _dummy, variable_name_length] = struct.unpack("<iiii", b)

        # Get the time of the first sample
        f.seek(variable_name_length, 1)  # Relative to current position
        b = f.read(8)
        start_time = struct.unpack("<d", b)[0]

    # Get time of last sample
    if n_samples != 0:
        # The normal case; file was closed correctly and the last sample in the
        # file was fully written (i.e., no half samples were written)
        with open(filename, "rb") as f:
            f.seek(-n_elements_per_sample * 8, 2)  # Seek relative to the end of the file
            b = f.read(8)
            end_time = struct.unpack("<d", b)[0]
    else:
        # Then the file was not closed correctly by TwinCAT and we need to work
        # out the data size ourselves
        r = CorrectCorruptMatlabFile.GetRelevantDataFromMatFile(filename)

        # We don't want to change the file, just read the data from the correct place
        with open(filename, "rb") as f:
            f.seek(20 + r['varnamelen'] + (r['nSamples'] - 1) * r[
                'nElementsPerSample'] * 8)  # Move pointer to start of last full sample
            b = f.read(8)
            end_time = struct.unpack("<d", b)[0]
    return end_time - start_time


def timestamp_from_human_readable(s):
    """Returns a time stamp (number) from a human-readable time stamp (e.g. 180608_140922)"""
    tt = datetime.datetime.strptime(s, "%y%m%d_%H%M%S").timestamp()
    return tt


def get_creation_date_time_string(filename):
    t = os.path.getctime(filename)
    return datetime.datetime.fromtimestamp(t).strftime("%y%m%d_%H%M%S")


def get_modification_date_time_string(filename, offset=0):
    """Returns the modification date as a timestring, with a given offset in seconds

    A positive offset makes the time later.
    """
    t = os.path.getmtime(filename)
    return datetime.datetime.fromtimestamp(t + offset).strftime("%y%m%d_%H%M%S")


def timestamp_to_humanreadable(t, include_date=True):
    if include_date:
        return datetime.datetime.fromtimestamp(t).strftime("%y%m%d_%H%M%S")
    else:
        return datetime.datetime.fromtimestamp(t).strftime("%H%M%S.%f")


def find_timestamps_in_dirs(dirs):
    """Returns all time stamps found in the list of directories _based on file name_

    The timestamps are converted to a numerical value first. If a directory is listed twice, in dirs, it is parsed
    only once.
    Only looks at the ALREADY_RENAMED_FILE_MASK files."""

    dirs = list(set(dirs))  # Remove duplicates
    dirs = [d for d in dirs if d is not None]  # Remove any None

    timestamps = []

    for d in dirs:
        all_files = glob(os.path.join(d, "*.*"))
        only_file_names = [os.path.split(f)[1] for f in all_files]
        all_interesting_files = [f for f in only_file_names if re.match(ALREADY_RENAMED_FILE_MASK, f)]

        for f in all_interesting_files:
            timestamps.append(timestamp_from_human_readable(f[0:13]))

    timestamps = list(set(timestamps))

    return timestamps


def find_timestamp_close_to_needle(needle, haystack, max_difference=10):
    """Given a set of timestamps (numerical) in haystack, and a single numerical timestamp in needle,
    returns the timestamp in haystack closest to needle, but only if there is one at most
    maxDifference seconds away. Otherwise, this function returns needle."""

    if len(haystack) == 0:
        return needle

    absdiff = [abs(h - needle) for h in haystack]
    minimum = absdiff.index(min(absdiff))
    match = (haystack[minimum], abs(haystack[minimum] - needle))

    if match[1] <= max_difference:
        return match[0]
    else:
        return needle


def get_signal_name_count(filename):
    """Returns the number of signal names in a SignalNames file.
    Returns None if the file does not exist."""

    def remove_comment_from_text_line(line):
        """Comment starts with % or # and holds till end of the line"""
        return re.split("[%#]", line)[0]

    ext = os.path.splitext(filename)[1]

    if not os.path.isfile(filename):
        return None

    if ext.lower() == ".mat":
        m = scipy.io.loadmat(filename)
        # Normally, m is a dict with four elements; keys are: '__header__','__version__','__globals__'
        # and a fourth one, which is the variable name of the list we're looking for. Let's get that
        # first by removing all other keys and taking the one remaining.
        m.pop('__header__')
        m.pop('__version__')
        m.pop('__globals__')

        # It may happen that the file has multiple variables (e.g., for the 3D Outputs). Just try our
        # luck and get the first variable...
        if len(m) != 1:
            wprint("SignalNames file %s has multiple variables.\n   Taking the first one (%s)" %
                   (filename, list(m.keys())[0]))

        n_signals = len(list(m.values())[0])
        return n_signals

    elif ext.lower() == ".txt":
        # Count the number of non-empty lines (after removing any comments)
        with open(filename, "rt") as f:
            s = f.readlines()
        # Each string in s now ends with "\n", it will be removed automatically with strip()
        # (which also removes any extra leading and trailing whitespaces)
        s = [remove_comment_from_text_line(ss).strip() for ss in s]
        n_signals = len([ss for ss in s if len(ss) != 0])
        return n_signals
    else:
        raise FileNotFoundError("Unknown file format for SignalNames file %s" % filename)


def copy_signal_names_file(from_name, to_name, current_datetime, directory):
    """Copies the SignalNames file to the data directory. The most important part is to resolve the
    original SignalNamesFile name and the new name. Both fromName and toName are file
    names with the full (absolute) paths.

    SignalNames is only copied _iff_ there is no SignalNames file yet for this series

    For now, we'll just copy the first file found in the list; this should be changed to copying
    the file with the right amount of signal names."""

    # We need the number of signals in the data file so that we can copy the correct SignalsFile
    # The data file contains all logged signals plus a time signal. Therefore, the matching
    # SignalsFile (which does *not* list the time signal has one signal less than the data file. Hence
    # the -1. We can use the n_signals directly as key for the file selection.
    try:
        n_signals = get_n_signals(to_name) - 1
    except FileNotFoundError:
        wprint("Could not find file %s.\n   Not trying to copy the SignalNames file." % to_name, indent=3)
        return

    # Find out the original file name. In the Configuration file, we have file prefixes (like
    # "WE2_JointData"). We want to allow both without trailing "_" (which is more logical) and with
    # trailing "_" (which is how the file name is specified in TwinCAT. Solution is to ignore all
    # trailing "_" for all sources. For the configuration, this was already done in the
    # ReadSignalNamesConfigurationFile function.
    base_name = re.split(r'_?part[0-9]*\.mat$', os.path.split(from_name)[1])[0]  # Removes _ as well

    # Find out if a Signals file is provided for the given type and remember its extension
    if base_name not in SIGNALNAMESCONFIG.keys():
        # No match for the file name
        iprint(current_datetime + directory)
        wprint("No SignalNames file provided for data file %s.\n"
               "   Please update ArchiveMeasurementsSignals.ini;\n"
               "   add a section [%s] in there." %
               (os.path.split(from_name)[1], base_name), indent=3)
        return

    # If we're here, have a match for the file name.
    # Also test for number of signals
    if n_signals not in SIGNALNAMESCONFIG[base_name].keys():
        # Make a nice warning text
        s1 = ("No SignalNames file provided with the correct amount of signal names\nfor file %s.\n"
              "   Expected a SignalNames file with %i entries, but only found the following:\n" %
              (os.path.split(from_name)[1], n_signals))

        s2 = "\n".join([("      %-5i" % k if isinstance(k, int) else "      WRONG") + " " + v for (k, v) in
                        SIGNALNAMESCONFIG[base_name].items()])
        wprint(s1 + s2, indent=3)
        return

    # If we're here, we have a match for file name and number of signals, so proceed copying.
    source_file = SIGNALNAMESCONFIG[base_name][n_signals]
    ext = os.path.splitext(source_file)[1]

    # Find out target name. This is simple; replace the 'partXX' by 'signalnames' and give it the
    # extension of the original signalnames file
    target_name = re.split(r'part[0-9]*\.mat$', to_name)[0] + "signalnames" + ext

    # Try to copy the file. shutil.copy does not detect if the file is already there so we do that manually
    if not os.path.isfile(target_name):
        try:
            shutil.copy(source_file, target_name)
            iprint("   SignalsFile %s\n   Copied to   %s" % (source_file, target_name))
        except Exception as E:
            wprint("   Tried to copy SignalNames file %s to %s, but failed (%s)" % (source_file, target_name, E))
    else:
        # The file was already copied, so do nothing
        pass


def format_datetime_iso_utc(date):
    """Take datetime object and print in ISO format, with UTC timezone (including `Z`)"""

    date = date.astimezone()  # Add system timezone
    date = date.astimezone(datetime.timezone.utc)  # Convert to UTC time
    date_str = date.isoformat()  # "+0:00" is included instead of 'Z'
    date_str = date_str[:-6] + 'Z'
    return date_str


def save_windows_events(experiment_start, current_datetime, to_file, watch_dir):
    """Save specific Windows events to a file

    The events matching to the config key events.query are pulled from the Windows event manager. The events between
    the start of the experiment and the current datetime are logged. If an event log for this experiment already exists
    it will be replaced (but as the timespan increases the events should overlap).

    The Windows command `wevtutil qe` is used to get a list of events.

    Input arguments are awful to be compatible with the rest of the script.

    :param experiment_start     Start of the experiment (number, for some reason...)
    :param current_datetime     Current datetime (as text, for some reason...)
    :param to_file              File that was moved most recently
    :param watch_dir            Original directory of the last moved file (in case destination is missing)
    """

    if not EVENTS_QUERY:
        return False  # Do nothing

    # Example command:
    # wevtutil qe Application "/q:*[System[TimeCreated[@SystemTime>='2020-09-15T00:00:00' and
    #       @SystemTime<='2020-09-15T23:59:59']][Provider[@Name='TcSysSrv' or @Name='TcEventLogger']]]" /f:text

    date_from = datetime.datetime.fromtimestamp(experiment_start) - datetime.timedelta(seconds=3)
    # Add a margin of a few seconds to make sure we get the 'starting...' event

    date_to = datetime.datetime.strptime(current_datetime[0:13], '%y%m%d_%H%M%S')  # There is a space at the end

    datetime_filter = '[TimeCreated[@SystemTime>=\'{}\' and @SystemTime<=\'{}\']]'\
        .format(format_datetime_iso_utc(date_from), format_datetime_iso_utc(date_to))
    # For some reason the datetime _has_ to be in UTC zone incl. the ISO 'Z'
    query = EVENTS_QUERY.format(datetime_filter)
    command = 'wevtutil qe Application "/q:*[{}]" /f:text'.format(query)

    iprint('   Getting events with `{}`'.format(command))

    handle = os.popen(command)
    processor = EventProcessor(handle)

    path, filename = os.path.split(to_file)

    if not os.path.exists(path):
        # Fall back on watch directory if destination was not found
        path = watch_dir

    event_filepath = os.path.join(path, filename[0:13] + '_WindowsEvents.txt')

    try:
        with open(event_filepath, 'w') as fh:
            i = 0
            for line in processor:
                fh.write(line)
                fh.write('\n')
                i += 1
            iprint("   Wrote %d events to   %s" % (i, event_filepath))
    except Exception as E:
        wprint("   Tried to write events file to `%s`, but failed (%s)" % (event_filepath, E))

    return True  # Event log successful


def run_rename(directory):
    """Does the whole rename thing. This function should be called regularly.

    :param directory    Current watch directory
    """

    # Print header
    current_date_time = datetime.datetime.now().strftime("%y%m%d_%H%M%S") + " "

    # Get file list. Internally, we need to update this variable to reflect the state, in order to be able
    # to do a dry-run.
    all_files = glob(os.path.join(directory, "*.*"))

    # Sometimes TwinCAT appears to make a file that is shorter than the full header. If so,
    # this script will crash. In order to avoid this, we need to ignore the file. To do so,
    # we just throw out all files smaller than 16 bytes from allFiles. This is not a very water-tight
    # test, since we don't know exactly how large the header is, because the variable name
    # can change in length. But hopefully it will solve at least some of the problems.
    all_files = [f for f in all_files if os.path.getsize(f) > 16]
    only_file_names = [os.path.split(f)[1] for f in all_files]

    all_interesting_files = [full for (f, full) in zip(only_file_names, all_files)
                             if re.match(FILE_MASK, f) and not re.match(ALREADY_RENAMED_FILE_MASK, f)]

    if len(all_interesting_files) != 0:
        iprint(current_date_time + directory)

    changes = False  # Set to True when any file was actually moved

    for full in all_interesting_files:
        (dd, ff) = os.path.split(full)

        if not is_renamable(full):
            s = os.path.getsize(full)
            ctime = timestamp_to_humanreadable(os.path.getctime(full), False)
            mtime = timestamp_to_humanreadable(os.path.getmtime(full), False)
            iprint("   Still open  %s (ctime=%s; mtime=%s; %.1f MB)\n" % (ff, ctime, mtime, s / 1e6))
            continue

        s = os.path.getsize(full)
        ctime = timestamp_to_humanreadable(os.path.getctime(full), False)
        mtime = timestamp_to_humanreadable(os.path.getmtime(full), False)
        iprint("   Renaming    %s (ctime=%s; mtime=%s; %.1f MB)" % (ff, ctime, mtime, s / 1e6))

        # If we're here, we can rename, de-corrupt and move the file.
        start_of_experiment = get_time_of_start_of_experiment(full)

        all_timestamps = find_timestamps_in_dirs([directory, MOVETODIRECTORY])
        time_stamp_for_file = find_timestamp_close_to_needle(start_of_experiment, all_timestamps)

        t1 = timestamp_to_humanreadable(start_of_experiment)
        t2 = timestamp_to_humanreadable(time_stamp_for_file)
        d = -start_of_experiment + time_stamp_for_file
        iprint("   Exper start %s; Closest existing: %s, diff: %f s" % (t1, t2, d))

        from_name = full
        to_name = os.path.join(dd, timestamp_to_humanreadable(time_stamp_for_file) + "_" + ff)

        # Update allFiles
        all_files[all_files.index(from_name)] = to_name

        # Do the actual rename if needed
        try:
            os.rename(from_name, to_name)
            iprint("   Renamed to  %s" % os.path.split(to_name)[1])
            changes = True
        except PermissionError as E:
            wprint("   Tried to rename, but file %s still open, not renaming (Exception: %s)." % (from_name, str(E)))

        if MOVETODIRECTORY is not None and directory.lower() != MOVETODIRECTORY.lower():
            # If destination if different from the source
            # Does the file fit easily? (we want to leave some space left for the log file)
            try:
                file_size = os.path.getsize(to_name)
                disk_free = shutil.disk_usage(MOVETODIRECTORY).free
                min_free_space_required = 5e6  # bytes

                if file_size < disk_free - min_free_space_required:
                    shutil.move(to_name, MOVETODIRECTORY)

                    iprint("   Moved to    %s" % MOVETODIRECTORY)
                else:
                    # Print different warnings dependent on disk free size
                    if file_size > disk_free:
                        reason = " (required: %d; free: %d)" % (file_size, disk_free)
                    else:
                        reason = "(after moving there would be %d bytes left)" % (disk_free - file_size)

                    wprint("Did not move %s to\n   %s because drive is almost full %s" %
                           (os.path.split(to_name)[1], MOVETODIRECTORY, reason), indent=3)

            except Exception as E:
                wprint("   Tried to move %s to %s, but failed (%s)" % (os.path.split(to_name)[1], MOVETODIRECTORY, E))

        if MOVETODIRECTORY is not None:
            new_file_name = os.path.join(MOVETODIRECTORY, os.path.split(to_name)[1])
        else:
            new_file_name = os.path.join(directory, os.path.split(to_name)[1])  # no memory check for original drive

        # Copy signal names iff there isn't one yet for this series
        copy_signal_names_file(from_name, new_file_name, current_date_time, directory)

        if changes:
            # Create an adjacent log of Windows events which are related to TwinCAT
            # Do only once per loop, not for every interesting file
            save_windows_events(start_of_experiment, current_date_time, new_file_name, dd)

        iprint("")


# TestLogger
# from PySide import QtCore

# class TestLogger(QtCore.QObject):
#     """Constructor arguments:
#     - Directory
#     - pre: first part of the file name
#     - time (seconds) between creation of files
#     Use Start() and Stop() to start/stop logging.
#
#     Note that this test logger will not work anymore. ArchiveMeasurements has become so sophisticated
#     that it reads all kinds of data from the data files; therefore the data files should be consistent
#     Matlab files now, which is not the case for the files made with this TestLogger. I'll leave the
#     TestLogger in for now however, so that, if someone wants, he can adapt it to produce meaningful
#     data files."""
#
#     def __init__(self, dir, pre, deltat):
#         self._dir = dir
#         self._pre=pre
#         self._deltat = deltat
#         self._isRunning = False
#         self._currentFile = None
#         self._n = 0
#         self._T = QtCore.QTimer()
#         self._T.timeout.connect(self._onTimer)
#
#     def Start(self):
#         if self._isRunning:
#             print("Already started. Doing nothing.")
#             return
#         self._n = 0
#         self._openNewFile(0)
#         self._T.start(self._deltat*1000)
#         self._isRunning = True
#         print("Started making log files.")
#
#     def Stop(self):
#         if not self._isRunning:
#             print("Already stopped.  Doing nothing.")
#             return
#         self._T.stop()
#         self._closeFile()
#         self._isRunning = False
#         print("Stopped making log files.")
#
#     def _onTimer(self):
#         self._closeFile()
#         self._n = self._n+1
#         self._openNewFile(self._n)
#
#     def _openNewFile(self,n):
#         if self._currentFile is None:
#             filename = os.path.join(self._dir, self._pre+"part"+str(n)+".mat")
#             self._currentFile = open(filename,"w")
#             self._currentFile.write("Just some test file.")
#             print ("Created "+os.path.split(filename)[1])
#         else:
#             print("_openNewFile was called while another file was open! Stopping timer.")
#             self._T.stop()
#
#     def _closeFile(self):
#         if self._currentFile is not None:
#             self._currentFile.close()
#             self._currentFile = None
#         else:
#             print("_closeFile was called without an open file! stopping timer.")
#             self._T.stop()
#
#
# L1 = TestLogger("d:\\WE1_logs","mytest_",9)
# L2 = TestLogger("d:\\WE1_logs","var_",12)


# Main program
if __name__ == '__main__':

    # Print startup message
    CONFIG = read_configuration_file()
    DIRECTORIES = CONFIG["paths"]
    MOVETODIRECTORY = CONFIG["movetopath"]
    EVENTS_QUERY = CONFIG["events_query"]

    CurrentDateTime = datetime.datetime.now().strftime("%y%m%d_%H%M%S")
    if MOVETODIRECTORY is not None:
        LOG_FILENAME = os.path.join(MOVETODIRECTORY, CurrentDateTime + "_ArchiveMeasurementsLog.txt")
    else:
        LOG_FILENAME = os.path.join(CurrentDateTime + "_ArchiveMeasurementsLog.txt")
    iprint("\n\n" + 80 * "=")
    iprint(CurrentDateTime + " Started ArchiveMeasurements")
    iprint(80 * "=")

    SIGNALNAMESCONFIG = read_signal_names_configuration_file()

    iprint("Archiving directories:")
    iprint("\n".join(DIRECTORIES), indent=3)
    iprint("Moving completed files to:")
    iprint(MOVETODIRECTORY, indent=3)
    iprint(80 * "-")

    try:
        while True:
            for directory_i in DIRECTORIES:
                run_rename(directory_i)
            time.sleep(15)  # 15 seconds
    except Exception as Err:
        # log the exception
        eprint("EXCEPTION!!")
        eprint(traceback.format_exc())
        raise
