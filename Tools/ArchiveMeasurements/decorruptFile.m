function [ ] = decorruptFile( file_path )
%DECORRUPTFILE decorrupts a data file file_path with the python script
%   Function only works, if we1_experiments/Tools is on the path, and the
%   python path in matlab is set
%   INPUT: file_path: full path of the corrupted file
    
    func_path = mfilename('fullpath');
    cur_path = cd(func_path(1:end-13));
    tmp_arg = py.list({0, file_path});
    py.CorrectCorruptMatlabFile.main(tmp_arg)
    cd(cur_path);
end

