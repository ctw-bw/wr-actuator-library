# Tools

This directory contains programs or apps that are useful for the TwinCAT projects but run independently.

Tools with multiple files should be in their own sub directory.

## Index

 - **ArchiveMeasurements** - A Python script to move and rename the files created by the TwinCAT file writer. Also contains a tool to fix corrupt MATLAB files.
 - **ProcessData** - A set of MATLAB functions and a script to load logged data from the TwinCAT file writer. The outcome of these functions are neat MATLAB tables of data, listed errors, events, etc.
