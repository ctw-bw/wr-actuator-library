%
% Below are unit tests for the `actuator_statemachine` method.
%

clear;
clear actuator_statemachine

Ts = 1.0 / 1000.0;

%% Test enable and disable

output = array2table(zeros(5000, 3), ...
    'VariableNames', {'ControlWord', 'DriveShouldBeEnabled', 'LatchJointError_Rising'});

for i = 1:height(output)
    
    t = 5 + i * Ts;
    
    enable = 0;
    disable = 0;
    if i == 1000
        enable = 1;
    end
    if i == 4000
        disable = 1;
    end
    
    [output.ControlWord(i), output.DriveShouldBeEnabled(i), output.LatchJointError_Rising(i)] = ...
        actuator_statemachine(enable, disable, 1, 0, 0, 0, t);
    
end

clear actuator_statemachine

assert(all(output.ControlWord(1:999) == 6), 'State does not start in shutdown');
assert(all(output.ControlWord(1000:1010) == 6 + 128), 'Missing fault reset');
assert(all(output.ControlWord(1050:1100) == 7), 'Missing switch-on');
assert(all(output.ControlWord(1200:3999) == 15), 'Not in operation');
assert(all(output.ControlWord(4000:end) == 6), 'Not in shutdown near the end');

assert(all(output.DriveShouldBeEnabled(1200:3999) == 1), 'Not enabled');

assert(all(output.LatchJointError_Rising([1:999 1200:3999 4200:end]) == 0), 'Should not latch error');
assert(all(output.LatchJointError_Rising([1050:1100 4000:4010]) == 1), 'Should latch error');

%% Test error detection

output = array2table(zeros(1000, 3), ...
    'VariableNames', {'ControlWord', 'DriveShouldBeEnabled', 'LatchJointError_Rising'});

for i = 1:height(output)
    
    t = 5 + i * Ts;
    
    enable = 0;
    joint_error = 0;
    if i == 1
        enable = 1;
    end
    if i == 500
        joint_error = 32;
    end
    
    [output.ControlWord(i), output.DriveShouldBeEnabled(i), output.LatchJointError_Rising(i)] = ...
        actuator_statemachine(enable, 0, 1, joint_error, 0, 0, t);
    
end

clear actuator_statemachine

assert(all(output.ControlWord(200:499) == 15), 'Not in operation');
assert(all(output.ControlWord(500:end) == 6), 'Not in shutdown near the end');
assert(all(output.LatchJointError_Rising([200:499 550:end]) == 0), 'Should not latch error');
assert(all(output.LatchJointError_Rising(500:510) == 1), 'Should latch error');

%% Test error during enable

output = array2table(zeros(1000, 3), ...
    'VariableNames', {'ControlWord', 'DriveShouldBeEnabled', 'LatchJointError_Rising'});

for i = 1:height(output)
    
    t = 5 + i * Ts;
    
    enable = 0;
    joint_error = 0;
    if i == 1
        enable = 1;
    end
    if i == 100 % Should be during STATE_SWITCHED_ON
        joint_error = 32;
    end
    
    [output.ControlWord(i), output.DriveShouldBeEnabled(i), output.LatchJointError_Rising(i)] = ...
        actuator_statemachine(enable, 0, 1, joint_error, 0, 0, t);
    
end

clear actuator_statemachine

assert(all(output.DriveShouldBeEnabled == 0), 'Should never be in operation');

assert(all(output.LatchJointError_Rising(100:105) == 1), 'Should latch error');
assert(all(output.LatchJointError_Rising(120:end) == 0), 'Should not latch error');

%% Test regular fault reset

output = array2table(zeros(3000, 3), ...
    'VariableNames', {'ControlWord', 'DriveShouldBeEnabled', 'LatchJointError_Rising'});

for i = 1:height(output)
    
    t = 5 + i * Ts;
    
    joint_error = 32; % Start with some error
    
    [output.ControlWord(i), output.DriveShouldBeEnabled(i), output.LatchJointError_Rising(i)] = ...
        actuator_statemachine(0, 0, 1, joint_error, 1000, 300, t);
    
end

clear actuator_statemachine

assert(all(output.DriveShouldBeEnabled == 0), 'Should never be in operation');

assert(all(output.ControlWord([1001 2002]) == 6 + 128), 'Should contain fault reset');
assert(all(output.ControlWord([1:1000 1002:2001 2003:end]) == 6), 'Should not contain fault reset');

%% Test enabled fault reset

output = array2table(zeros(1300, 3), ...
    'VariableNames', {'ControlWord', 'DriveShouldBeEnabled', 'LatchJointError_Rising'});

for i = 1:height(output)
    
    t = 5 + i * Ts;
    
    enable = 0;
    if i == 1
        enable = 1;
    end
    
    [output.ControlWord(i), output.DriveShouldBeEnabled(i), output.LatchJointError_Rising(i)] = ...
        actuator_statemachine(enable, 0, 1, 0, 1000, 300, t);
    
end

clear actuator_statemachine

assert(all(output.DriveShouldBeEnabled(200:end) == 1), 'Should be in operation');

assert(all(output.ControlWord([474 775 1076]) == 15 + 128), 'Should contain fault reset');
assert(all(output.ControlWord([200:473 475:774 776:1075 1077:end]) == 15), 'Should not contain fault reset');
