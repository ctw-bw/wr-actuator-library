% WE2_library_init.m
%
% This script is automatically called when WE2_library.slx is used.
% It creates some required parameters/variables and bus structures and does
% some basic safety tests on the parameters.

%% Set path

[directory, ~, ~] = fileparts(mfilename('fullpath'));

addpath(fullfile(directory, 'ParameterFiles'));
addpath(fullfile(directory, 'Functions'));

%% Read Joint parameters
load_joint_parameters;

%% Create bus structures for TwinCAT inputs/outputs
% First we create a struct with the required fields of the correct type;
% variable value is irrelevant. Then we call the struct2bus function to get
% a bus object.
ActuatorInStruct = struct;
ActuatorInStruct.StatusWord = uint16(0);
ActuatorInStruct.STOStatus = uint16(0);
ActuatorInStruct.CurrentLoopStatus = uint32(0);
ActuatorInStruct.LastError = int32(0);
ActuatorInStruct.GPI = uint32(0);
ActuatorInStruct.BusVoltageValue = single(0);
ActuatorInStruct.TorqueActualValue = int16(0);
ActuatorInStruct.PositionActualValue = int32(0);
ActuatorInStruct.VelocityActualValue = int32(0);
ActuatorInStruct.OutputEncoderValue = int32(0);
ActuatorInStruct.SpringEncoderValue = int32(0);

ActuatorIn = struct2bus(ActuatorInStruct);
clear ActuatorInStruct

WE_InputBus_Struct = struct;
WE_InputBus_Struct.DriveIn = ActuatorIn;
WE_InputBus_Struct.EtherCATState = uint16(0); % Not from drive, but from TwinCAT

WE_InputBus = struct2bus(WE_InputBus_Struct);
WE_InputBus.Elements(1).DataType = 'Bus: ActuatorIn'; % Overwrite type, it won't be detected
clear WE2_InputBus_Struct

WE_OutputBus_Struct.ControlWord = uint16(0);
WE_OutputBus_Struct.ModesOfOperation = int8(0);
WE_OutputBus_Struct.TargetTorque = int16(0);
WE_OutputBus_Struct.GPO = uint32(0);
WE_OutputBus = struct2bus(WE_OutputBus_Struct);
clear WE_OutputBus_Struct


%% To_ToActuatorBus
% Direct inputs from TwinCAT
TTBus.StatusWord = double(0);
TTBus.PositionActualValue_counts = double(0);
TTBus.VelocityActualValue_IU = double(0);
TTBus.OutputEncoderValue_counts = double(0);
TTBus.SpringEncoderValue_counts = double(0);
TTBus.TorqueActualValue_counts = double(0);
TTBus.GPI = double(0);
TTBus.BusVoltageValue_V = double(0);
TTBus.LastError = double(0);
TTBus.STOStatus = double(0);
TTBus.CurrentLoopStatus = double(0);
TTBus.EtherCATState = double(0); % Not from drive but from TwinCAT
% Derived values
TTBus.OperationEnabled = double(0);
TTBus.MotorAngle_rad = double(0);
TTBus.MotorVelocity_rad_s = double(0);
TTBus.JointAngle_rad = double(0);
TTBus.JointVelocity_rad_s = double(0);
TTBus.SpringDeflection_rad = double(0);

                                     % Bits: 31-12  | 11  10   9   8  |  7   6   5   4  |  3   2   1   0  |
                                     %      -       |  Motor encoder  |  Joint encoder  | Spring encoder  |
TTBus.EncoderSpikeInfo = double(0);  %      -       |  -  Ext Rep Spk |  -  Ext Rep Spk |  -  Ext Rep Spk |
TTBus.MotorTorque_Nm = double(0);
TTBus.JointTorque_Nm = double(0);
TTBus.OutputEncoderOffset = double(0);
WE_ToToActuatorBus = struct2bus(TTBus);
clear TTBus

%% WRBS-A Input Bus
WRBSABus.Status = uint16(0);
WRBSABus.DigIn = uint16(0);
WRBSABus.V_Right = uint16(0);
WRBSABus.V_Left = uint16(0);
WRBSABus.V_Comp = uint16(0);
WRBSABus.V_Aux5 = uint16(0);
WRBSABus.V_Aux12 = uint16(0);
WRBSABus.V_Us = uint16(0);
WRBSABus.V_Up = uint16(0);
WRBSABus.V_Board = uint16(0);
WRBSABus.I_Right = uint16(0);
WRBSABus.I_Left = uint16(0);
WRBSABus.I_Comp = uint16(0);
WRBSABus.I_Aux5 = uint16(0);
WRBSABus.I_Aux12 = uint16(0);
WRBSABus.I_Us = uint16(0);
WRBSABus.I_Up = uint16(0);
WRBSABus.I_Dump = uint16(0);
WRBSABus.Temperature = uint16(0);
WE_WRBSABus = struct2bus(WRBSABus);
clear WRBSABus

%% BackpackInputBus
% Analog Beckhoff modules have a size of 2 bytes not 4
BackpackBus.FootContactLeftHeel_counts = int16(0);
BackpackBus.FootContactLeftToe_counts = int16(0);
BackpackBus.FootContactRightHeel_counts = int16(0);
BackpackBus.FootContactRightToe_counts = int16(0);
BackpackBus.CrutchHandbrakePushButton_counts = int16(0);
BackpackBus.CrutchNearButton_counts = int16(0);
BackpackBus.CrutchFarButton_counts = int16(0);
BackpackBus.CrutchThumbButton_counts = int16(0);
BackpackBus.MotorPowerControlSignal_counts = int16(0);
BackpackBus.MotorBattery_counts = int16(0);
BackpackBus.LogicBattery_counts = int16(0);
BackpackBus.TemperatureSensorTopVoltage_counts = int16(0);
BackpackBus.TemperatureSensorBottomVoltage_counts = int16(0);
WE_BackpackBus = struct2bus(BackpackBus);

%% ForcePlateBus
FPBus.Input1 = int16(0);
FPBus.Input2 = int16(0);
FPBus.Input3 = int16(0);
FPBus.Input4 = int16(0);
FPBus.Input5 = int16(0);
FPBus.Input6 = int16(0);
FPBus.Input7 = int16(0);
FPBus.Input8 = int16(0);

WE_ForcePlateBus = struct2bus(FPBus);
clear FPBus

%% IMUBus
IMUBus.AccX = single(0);
IMUBus.AccY = single(0);
IMUBus.AccZ = single(0);
IMUBus.AccState = uint8(0);
IMUBus.GyrX = single(0);
IMUBus.GyrY = single(0);
IMUBus.GyrZ = single(0);
IMUBus.GyrState = uint8(0);

WE_IMUBus = struct2bus(IMUBus);
clear IMUBus 

%% SysTimeBus
SysTimeBus.time_0_31 = uint32(0);
SysTimeBus.time_32_63 = int32(0);

WE_SysTimeBus = struct2bus(SysTimeBus);
clear SysTimeBus 

%% Check if the signs of MotorEncoderCounts_rev and MotorConstant match and if MaxMotorTorque_Nm is positive
%
% COMMENTED OUT BY GvO BECAUSE IT IS INCOMPATIBLE WITH INGENIA PARAMETER
% SET. TODO: WHEN ALL JOINTS ARE CONVERTED, CONVERT THIS PART TO INGENIA
% PARAMETERS AND UNCOMMENT.
%
% joints=fields(Joint_params);
% for i=1:length(joints)
%     joint=joints{i};
%     if sign(Joint_params.(joint).MotorEncoderCounts_rev) ~=sign(Joint_params.(joint).MotorConstant)
%         Joint_params.([joint '_ERROR']) = Joint_params.(joint);
%         Joint_params = rmfield(Joint_params,joint);
%         warning('MotorEncoderCounts_rev and MotorConstant don''t have the same sign for joint ''%s''. Renamed offending joint to make sure you can''t run the model.', joint)
%         continue;
%     end
% end
clear joints i joint